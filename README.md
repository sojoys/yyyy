# 此项目已废弃，最新游戏服务框架[Artifact](https://gitee.com/sojoys/artifact)
QQ群:258601671


##SandKing能做什么？##
一个快速开发Java游戏的一个框架

开发人员不用再自己去写繁琐的数据、缓存代码；

不用再去和客户端程序频繁的对指令

---
##能否用于实际开发中？##
暂时还不能用于实际开发中！还有很多功能未开发完成！

数据层基本开发都已完毕

要是您有什么好的想法可以`225880493`群！

---
###SandKing主要包括3个方面
* 网络指令生成
* 数据层代码生成
* 数据文件解析生成

还包括很多常用的常用工具类！

####数据层生成器
**使用**

打开`com.sandking.config.SK_Config`进行数据库配置

    public static final String URL = "jdbc:mysql://127.0.0.1:3306/hoc?autoReconnect=true&characterEncoding=utf-8";
    public static final String USERNAME = "root";
	public static final String PASSWORD = "";
	public static final String DRIVER = "com.mysql.jdbc.Driver";

打开`com.sandking.metadata.Test`直接运行

    SK_Generate.run(SK_Config.getDataSource(), new SK_Config(),"src/com/sandking/db", SK_Generate.getConfiguration());
                
代码生成在`com.sandking.db`包下

**说明**

在DB包中生成了4个包 bean、cache、dao、imp 。根据`数据库关系生成`。索引、主外键生成了一系列的查询函数和数据库操作

`bean`     下面是生成的与数据库对应的POJO对象

`cache`    下面是进程级内存的封装

`dao`      是对数据库操作的封装

`imp`      是用来自定义扩展的

`cfg`      配置库生成的操作相关类

`jedis`    和DAO类似的Redis操作封装