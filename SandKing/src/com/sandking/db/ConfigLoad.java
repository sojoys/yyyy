package com.sandking.db;

import com.sandking.db.cfg.*;
import com.sandking.tools.SK_Plus;

/**
 * 加载所有的配置
 */
public class ConfigLoad {
	public static boolean loadAllConfig(String path) throws Exception{
		// 兵营队列
		BydlCfg.loadFile(SK_Plus.b(path, "Bydl.sk").e());
		
		// 建筑队列
		JzdlCfg.loadFile(SK_Plus.b(path, "Jzdl.sk").e());
		
		// 服务器
		FwqCfg.loadFile(SK_Plus.b(path, "Fwq.sk").e());
		
		// 用户
		YhCfg.loadFile(SK_Plus.b(path, "Yh.sk").e());
		
		// 用户建筑
		YhjzCfg.loadFile(SK_Plus.b(path, "Yhjz.sk").e());
		
		// 用户映射
		YhysCfg.loadFile(SK_Plus.b(path, "Yhys.sk").e());
		
		// 用户类型
		YhlxCfg.loadFile(SK_Plus.b(path, "Yhlx.sk").e());
		
		// 用户背包
		YhbbCfg.loadFile(SK_Plus.b(path, "Yhbb.sk").e());
		
		// 用户英雄
		YhyxCfg.loadFile(SK_Plus.b(path, "Yhyx.sk").e());
		
		// 用户装饰
		YhzsCfg.loadFile(SK_Plus.b(path, "Yhzs.sk").e());
		
		// 用户邮件
		YhyjCfg.loadFile(SK_Plus.b(path, "Yhyj.sk").e());
		
		// 用户陷阱
		YhxjCfg.loadFile(SK_Plus.b(path, "Yhxj.sk").e());
		
		// 用户障碍
		YhzaCfg.loadFile(SK_Plus.b(path, "Yhza.sk").e());
		
		// 聊天
		LtCfg.loadFile(SK_Plus.b(path, "Lt.sk").e());
		
		// 聊天类型
		LtlxCfg.loadFile(SK_Plus.b(path, "Ltlx.sk").e());
		
		// 联盟
		LmCfg.loadFile(SK_Plus.b(path, "Lm.sk").e());
		
		// 英雄技能
		YxjnCfg.loadFile(SK_Plus.b(path, "Yxjn.sk").e());
		
		// 英雄状态
		YxztCfg.loadFile(SK_Plus.b(path, "Yxzt.sk").e());
		
		// 邮件类型
		YjlxCfg.loadFile(SK_Plus.b(path, "Yjlx.sk").e());
		
		// 邮件附件
		YjfjCfg.loadFile(SK_Plus.b(path, "Yjfj.sk").e());
		
		// 附件类型
		FjlxCfg.loadFile(SK_Plus.b(path, "Fjlx.sk").e());
		
		// 障碍队列
		ZadlCfg.loadFile(SK_Plus.b(path, "Zadl.sk").e());
		
		return true;
	}
	
	public static boolean writeAllConfig(String path) throws Exception{
		// 兵营队列
		BydlCfg.writeFile(SK_Plus.b(path, "Bydl.sk").e());
		
		// 建筑队列
		JzdlCfg.writeFile(SK_Plus.b(path, "Jzdl.sk").e());
		
		// 服务器
		FwqCfg.writeFile(SK_Plus.b(path, "Fwq.sk").e());
		
		// 用户
		YhCfg.writeFile(SK_Plus.b(path, "Yh.sk").e());
		
		// 用户建筑
		YhjzCfg.writeFile(SK_Plus.b(path, "Yhjz.sk").e());
		
		// 用户映射
		YhysCfg.writeFile(SK_Plus.b(path, "Yhys.sk").e());
		
		// 用户类型
		YhlxCfg.writeFile(SK_Plus.b(path, "Yhlx.sk").e());
		
		// 用户背包
		YhbbCfg.writeFile(SK_Plus.b(path, "Yhbb.sk").e());
		
		// 用户英雄
		YhyxCfg.writeFile(SK_Plus.b(path, "Yhyx.sk").e());
		
		// 用户装饰
		YhzsCfg.writeFile(SK_Plus.b(path, "Yhzs.sk").e());
		
		// 用户邮件
		YhyjCfg.writeFile(SK_Plus.b(path, "Yhyj.sk").e());
		
		// 用户陷阱
		YhxjCfg.writeFile(SK_Plus.b(path, "Yhxj.sk").e());
		
		// 用户障碍
		YhzaCfg.writeFile(SK_Plus.b(path, "Yhza.sk").e());
		
		// 聊天
		LtCfg.writeFile(SK_Plus.b(path, "Lt.sk").e());
		
		// 聊天类型
		LtlxCfg.writeFile(SK_Plus.b(path, "Ltlx.sk").e());
		
		// 联盟
		LmCfg.writeFile(SK_Plus.b(path, "Lm.sk").e());
		
		// 英雄技能
		YxjnCfg.writeFile(SK_Plus.b(path, "Yxjn.sk").e());
		
		// 英雄状态
		YxztCfg.writeFile(SK_Plus.b(path, "Yxzt.sk").e());
		
		// 邮件类型
		YjlxCfg.writeFile(SK_Plus.b(path, "Yjlx.sk").e());
		
		// 邮件附件
		YjfjCfg.writeFile(SK_Plus.b(path, "Yjfj.sk").e());
		
		// 附件类型
		FjlxCfg.writeFile(SK_Plus.b(path, "Fjlx.sk").e());
		
		// 障碍队列
		ZadlCfg.writeFile(SK_Plus.b(path, "Zadl.sk").e());
		
		return true;
	}
}