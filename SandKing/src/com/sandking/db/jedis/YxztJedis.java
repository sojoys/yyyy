package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.YxztDao;
import com.sandking.db.bean.Yxzt;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 英雄状态
 */
public class YxztJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxztJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yxzt yxzt = null;
		try{
			String key = "Yxzt_Object";
			String field = SK_Plus.b("id:",id).e();
			yxzt = Yxzt.createForJson(jedis.hget(key,field));
			if(yxzt == null){
				yxzt = YxztDao.getById(id);
				if(yxzt!=null){
					loadCache(yxzt);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yxzt;
	}
	

	public static void loadCache(Yxzt yxzt){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yxzt,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yxzt yxzt,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yxzt_Object";
		String field = SK_Plus.b("id:",yxzt.getId()).e();
		String data = yxzt.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yxzt> yxzts){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yxzt yxzt : yxzts){
				loadCache(yxzt,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yxzt yxzt){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yxzt,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yxzt yxzt,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yxzt_Object";
		String field = SK_Plus.b("id:",yxzt.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yxzt> yxzts){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yxzt yxzt : yxzts){
				clearCache(yxzt,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yxzt insert(Yxzt yxzt){
		yxzt = YxztDao.insert(yxzt);
		if(yxzt!=null){
			loadCache(yxzt);
		}
    	return yxzt;
    }
    
    public static Yxzt update(Yxzt yxzt){
    	yxzt = YxztDao.update(yxzt);
    	if(yxzt!=null){
    		clearCache(yxzt);
			loadCache(yxzt);
		}
    	return yxzt;
    }
    
    public static boolean delete(Yxzt yxzt){
    	boolean bool = YxztDao.delete(yxzt);
    	if(bool){
    		clearCache(yxzt);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yxzt> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yxzt> yxzts = new ArrayList<Yxzt>();
		try{
			String key = "Yxzt_Object";
			List<String> jsons = jedis.hvals(key);
			yxzts = Yxzt.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yxzts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxzt> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yxzt> yxzts = new ArrayList<Yxzt>();
		try{
			if(!isLoadAll){
				yxzts = YxztDao.getAll();
				loadCaches(yxzts);
				isLoadAll = true;
			}else{
				String key = "Yxzt_Object";
				List<String> jsons = jedis.hvals(key);
				yxzts = Yxzt.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yxzts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxzt> getAllByPage(int page,int size,Integer pageCount){
		List<Yxzt> yxzts = getAll();
		yxzts = SK_List.getPage(yxzts, page, size, pageCount);
		return yxzts;
	}
}