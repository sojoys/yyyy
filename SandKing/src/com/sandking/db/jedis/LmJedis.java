package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.LmDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import com.sandking.db.bean.Lm;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 联盟
 */
public class LmJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LmJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Lm lm = null;
		try{
			String key = "Lm_Object";
			String field = SK_Plus.b("id:",id).e();
			lm = Lm.createForJson(jedis.hget(key,field));
			if(lm == null){
				lm = LmDao.getById(id);
				if(lm!=null){
					loadCache(lm);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return lm;
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lm> lms = new ArrayList<Lm>();
		try{
			String key = "Lm_Index";
			String field = SK_Plus.b(key,"cjrid:",cjrid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Lm_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				lms = Lm.createForJson(jsons);
			}else{
				lms = LmDao.getByCjrid(cjrid);
				if(lms!=null && !lms.isEmpty()){
					loadCaches(lms);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lms;
	}
	
	
	public static List<Lm> getByPageCjrid(int cjrid,int page,int size,Integer pageCount){
		List<Lm> lms = getByCjrid(cjrid);
		lms = SK_List.getPage(lms, page, size, pageCount);
		return lms;
	}
	

	public static void loadCache(Lm lm){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(lm,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Lm lm,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Lm_Object";
		String field = SK_Plus.b("id:",lm.getId()).e();
		String data = lm.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(lm.getId());
		key = "Lm_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"cjrid:",lm.getCjrid()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Lm> lms){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Lm lm : lms){
				loadCache(lm,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Lm lm){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(lm,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Lm lm,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Lm_Object";
		String field = SK_Plus.b("id:",lm.getId()).e();
		p.hdel(key,field);
		
		key = "Lm_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(lm.getId());
		field = SK_Plus.b(key,"cjrid:",lm.getCjrid()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Lm> lms){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Lm lm : lms){
				clearCache(lm,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Lm insert(Lm lm){
		lm = LmDao.insert(lm);
		if(lm!=null){
			loadCache(lm);
		}
    	return lm;
    }
    
    public static Lm update(Lm lm){
    	lm = LmDao.update(lm);
    	if(lm!=null){
    		clearCache(lm);
			loadCache(lm);
		}
    	return lm;
    }
    
    public static boolean delete(Lm lm){
    	boolean bool = LmDao.delete(lm);
    	if(bool){
    		clearCache(lm);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Lm> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lm> lms = new ArrayList<Lm>();
		try{
			String key = "Lm_Object";
			List<String> jsons = jedis.hvals(key);
			lms = Lm.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lms;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lm> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lm> lms = new ArrayList<Lm>();
		try{
			if(!isLoadAll){
				lms = LmDao.getAll();
				loadCaches(lms);
				isLoadAll = true;
			}else{
				String key = "Lm_Object";
				List<String> jsons = jedis.hvals(key);
				lms = Lm.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lms;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lm> getAllByPage(int page,int size,Integer pageCount){
		List<Lm> lms = getAll();
		lms = SK_List.getPage(lms, page, size, pageCount);
		return lms;
	}
}