package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.dao.YhlxDao;
import com.sandking.db.bean.Yhlx;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户类型
 */
public class YhlxJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhlxJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhlx yhlx = null;
		try{
			String key = "Yhlx_Object";
			String field = SK_Plus.b("id:",id).e();
			yhlx = Yhlx.createForJson(jedis.hget(key,field));
			if(yhlx == null){
				yhlx = YhlxDao.getById(id);
				if(yhlx!=null){
					loadCache(yhlx);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhlx;
	}
	

	public static void loadCache(Yhlx yhlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhlx yhlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhlx_Object";
		String field = SK_Plus.b("id:",yhlx.getId()).e();
		String data = yhlx.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhlx> yhlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhlx yhlx : yhlxs){
				loadCache(yhlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhlx yhlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhlx yhlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhlx_Object";
		String field = SK_Plus.b("id:",yhlx.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhlx> yhlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhlx yhlx : yhlxs){
				clearCache(yhlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhlx insert(Yhlx yhlx){
		yhlx = YhlxDao.insert(yhlx);
		if(yhlx!=null){
			loadCache(yhlx);
		}
    	return yhlx;
    }
    
    public static Yhlx update(Yhlx yhlx){
    	yhlx = YhlxDao.update(yhlx);
    	if(yhlx!=null){
    		clearCache(yhlx);
			loadCache(yhlx);
		}
    	return yhlx;
    }
    
    public static boolean delete(Yhlx yhlx){
    	boolean bool = YhlxDao.delete(yhlx);
    	if(bool){
    		clearCache(yhlx);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhlx> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhlx> yhlxs = new ArrayList<Yhlx>();
		try{
			String key = "Yhlx_Object";
			List<String> jsons = jedis.hvals(key);
			yhlxs = Yhlx.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhlx> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhlx> yhlxs = new ArrayList<Yhlx>();
		try{
			if(!isLoadAll){
				yhlxs = YhlxDao.getAll();
				loadCaches(yhlxs);
				isLoadAll = true;
			}else{
				String key = "Yhlx_Object";
				List<String> jsons = jedis.hvals(key);
				yhlxs = Yhlx.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhlx> getAllByPage(int page,int size,Integer pageCount){
		List<Yhlx> yhlxs = getAll();
		yhlxs = SK_List.getPage(yhlxs, page, size, pageCount);
		return yhlxs;
	}
}