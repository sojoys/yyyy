package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import com.sandking.db.bean.Yxjn;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
import com.sandking.db.dao.YxjnDao;
/**
 * 英雄技能
 */
public class YxjnJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxjnJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yxjn yxjn = null;
		try{
			String key = "Yxjn_Object";
			String field = SK_Plus.b("id:",id).e();
			yxjn = Yxjn.createForJson(jedis.hget(key,field));
			if(yxjn == null){
				yxjn = YxjnDao.getById(id);
				if(yxjn!=null){
					loadCache(yxjn);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yxjn;
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		try{
			String key = "Yxjn_Index";
			String field = SK_Plus.b(key,"yhyx_id:",yhyx_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yxjn_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yxjns = Yxjn.createForJson(jsons);
			}else{
				yxjns = YxjnDao.getByYhyx_id(yhyx_id);
				if(yxjns!=null && !yxjns.isEmpty()){
					loadCaches(yxjns);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yxjns;
	}
	
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,int page,int size,Integer pageCount){
		List<Yxjn> yxjns = getByYhyx_id(yhyx_id);
		yxjns = SK_List.getPage(yxjns, page, size, pageCount);
		return yxjns;
	}
	

	public static void loadCache(Yxjn yxjn){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yxjn,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yxjn yxjn,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yxjn_Object";
		String field = SK_Plus.b("id:",yxjn.getId()).e();
		String data = yxjn.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yxjn.getId());
		key = "Yxjn_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yhyx_id:",yxjn.getYhyx_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yxjn> yxjns){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yxjn yxjn : yxjns){
				loadCache(yxjn,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yxjn yxjn){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yxjn,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yxjn yxjn,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yxjn_Object";
		String field = SK_Plus.b("id:",yxjn.getId()).e();
		p.hdel(key,field);
		
		key = "Yxjn_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yxjn.getId());
		field = SK_Plus.b(key,"yhyx_id:",yxjn.getYhyx_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yxjn> yxjns){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yxjn yxjn : yxjns){
				clearCache(yxjn,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yxjn insert(Yxjn yxjn){
		yxjn = YxjnDao.insert(yxjn);
		if(yxjn!=null){
			loadCache(yxjn);
		}
    	return yxjn;
    }
    
    public static Yxjn update(Yxjn yxjn){
    	yxjn = YxjnDao.update(yxjn);
    	if(yxjn!=null){
    		clearCache(yxjn);
			loadCache(yxjn);
		}
    	return yxjn;
    }
    
    public static boolean delete(Yxjn yxjn){
    	boolean bool = YxjnDao.delete(yxjn);
    	if(bool){
    		clearCache(yxjn);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yxjn> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		try{
			String key = "Yxjn_Object";
			List<String> jsons = jedis.hvals(key);
			yxjns = Yxjn.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yxjns;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxjn> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		try{
			if(!isLoadAll){
				yxjns = YxjnDao.getAll();
				loadCaches(yxjns);
				isLoadAll = true;
			}else{
				String key = "Yxjn_Object";
				List<String> jsons = jedis.hvals(key);
				yxjns = Yxjn.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yxjns;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxjn> getAllByPage(int page,int size,Integer pageCount){
		List<Yxjn> yxjns = getAll();
		yxjns = SK_List.getPage(yxjns, page, size, pageCount);
		return yxjns;
	}
}