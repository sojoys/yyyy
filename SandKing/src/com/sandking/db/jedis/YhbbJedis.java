package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.YhbbDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.bean.Yhbb;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户背包
 */
public class YhbbJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhbbJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhbb yhbb = null;
		try{
			String key = "Yhbb_Object";
			String field = SK_Plus.b("id:",id).e();
			yhbb = Yhbb.createForJson(jedis.hget(key,field));
			if(yhbb == null){
				yhbb = YhbbDao.getById(id);
				if(yhbb!=null){
					loadCache(yhbb);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhbb;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		try{
			String key = "Yhbb_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhbb_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhbbs = Yhbb.createForJson(jsons);
			}else{
				yhbbs = YhbbDao.getByYh_id(yh_id);
				if(yhbbs!=null && !yhbbs.isEmpty()){
					loadCaches(yhbbs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhbbs;
	}
	
	
	public static List<Yhbb> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhbb> yhbbs = getByYh_id(yh_id);
		yhbbs = SK_List.getPage(yhbbs, page, size, pageCount);
		return yhbbs;
	}
	

	public static void loadCache(Yhbb yhbb){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhbb,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhbb yhbb,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhbb_Object";
		String field = SK_Plus.b("id:",yhbb.getId()).e();
		String data = yhbb.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhbb.getId());
		key = "Yhbb_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhbb.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhbb> yhbbs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhbb yhbb : yhbbs){
				loadCache(yhbb,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhbb yhbb){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhbb,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhbb yhbb,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhbb_Object";
		String field = SK_Plus.b("id:",yhbb.getId()).e();
		p.hdel(key,field);
		
		key = "Yhbb_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhbb.getId());
		field = SK_Plus.b(key,"yh_id:",yhbb.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhbb> yhbbs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhbb yhbb : yhbbs){
				clearCache(yhbb,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhbb insert(Yhbb yhbb){
		yhbb = YhbbDao.insert(yhbb);
		if(yhbb!=null){
			loadCache(yhbb);
		}
    	return yhbb;
    }
    
    public static Yhbb update(Yhbb yhbb){
    	yhbb = YhbbDao.update(yhbb);
    	if(yhbb!=null){
    		clearCache(yhbb);
			loadCache(yhbb);
		}
    	return yhbb;
    }
    
    public static boolean delete(Yhbb yhbb){
    	boolean bool = YhbbDao.delete(yhbb);
    	if(bool){
    		clearCache(yhbb);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhbb> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		try{
			String key = "Yhbb_Object";
			List<String> jsons = jedis.hvals(key);
			yhbbs = Yhbb.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhbbs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhbb> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		try{
			if(!isLoadAll){
				yhbbs = YhbbDao.getAll();
				loadCaches(yhbbs);
				isLoadAll = true;
			}else{
				String key = "Yhbb_Object";
				List<String> jsons = jedis.hvals(key);
				yhbbs = Yhbb.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhbbs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhbb> getAllByPage(int page,int size,Integer pageCount){
		List<Yhbb> yhbbs = getAll();
		yhbbs = SK_List.getPage(yhbbs, page, size, pageCount);
		return yhbbs;
	}
}