package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import com.sandking.db.bean.Yhyx;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.db.dao.YhyxDao;
import com.sandking.tools.SK_List;
/**
 * 用户英雄
 */
public class YhyxJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyxJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhyx yhyx = null;
		try{
			String key = "Yhyx_Object";
			String field = SK_Plus.b("id:",id).e();
			yhyx = Yhyx.createForJson(jedis.hget(key,field));
			if(yhyx == null){
				yhyx = YhyxDao.getById(id);
				if(yhyx!=null){
					loadCache(yhyx);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhyx;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		try{
			String key = "Yhyx_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhyx_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhyxs = Yhyx.createForJson(jsons);
			}else{
				yhyxs = YhyxDao.getByYh_id(yh_id);
				if(yhyxs!=null && !yhyxs.isEmpty()){
					loadCaches(yhyxs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyxs;
	}
	
	
	public static List<Yhyx> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getByYh_id(yh_id);
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
	
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		try{
			String key = "Yhyx_Index";
			String field = SK_Plus.b(key,"yxztid:",yxztid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhyx_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhyxs = Yhyx.createForJson(jsons);
			}else{
				yhyxs = YhyxDao.getByYxztid(yxztid);
				if(yhyxs!=null && !yhyxs.isEmpty()){
					loadCaches(yhyxs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyxs;
	}
	
	
	public static List<Yhyx> getByPageYxztid(int yxztid,int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getByYxztid(yxztid);
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
	

	public static void loadCache(Yhyx yhyx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhyx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhyx yhyx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhyx_Object";
		String field = SK_Plus.b("id:",yhyx.getId()).e();
		String data = yhyx.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhyx.getId());
		key = "Yhyx_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhyx.getYh_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"yxztid:",yhyx.getYxztid()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhyx> yhyxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhyx yhyx : yhyxs){
				loadCache(yhyx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhyx yhyx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhyx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhyx yhyx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhyx_Object";
		String field = SK_Plus.b("id:",yhyx.getId()).e();
		p.hdel(key,field);
		
		key = "Yhyx_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhyx.getId());
		field = SK_Plus.b(key,"yh_id:",yhyx.getYh_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"yxztid:",yhyx.getYxztid()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhyx> yhyxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhyx yhyx : yhyxs){
				clearCache(yhyx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhyx insert(Yhyx yhyx){
		yhyx = YhyxDao.insert(yhyx);
		if(yhyx!=null){
			loadCache(yhyx);
		}
    	return yhyx;
    }
    
    public static Yhyx update(Yhyx yhyx){
    	yhyx = YhyxDao.update(yhyx);
    	if(yhyx!=null){
    		clearCache(yhyx);
			loadCache(yhyx);
		}
    	return yhyx;
    }
    
    public static boolean delete(Yhyx yhyx){
    	boolean bool = YhyxDao.delete(yhyx);
    	if(bool){
    		clearCache(yhyx);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhyx> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		try{
			String key = "Yhyx_Object";
			List<String> jsons = jedis.hvals(key);
			yhyxs = Yhyx.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyx> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		try{
			if(!isLoadAll){
				yhyxs = YhyxDao.getAll();
				loadCaches(yhyxs);
				isLoadAll = true;
			}else{
				String key = "Yhyx_Object";
				List<String> jsons = jedis.hvals(key);
				yhyxs = Yhyx.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyx> getAllByPage(int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getAll();
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
}