package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import com.sandking.db.dao.YhzsDao;
import com.sandking.db.bean.Yhzs;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户装饰
 */
public class YhzsJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzsJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhzs yhzs = null;
		try{
			String key = "Yhzs_Object";
			String field = SK_Plus.b("id:",id).e();
			yhzs = Yhzs.createForJson(jedis.hget(key,field));
			if(yhzs == null){
				yhzs = YhzsDao.getById(id);
				if(yhzs!=null){
					loadCache(yhzs);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhzs;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		try{
			String key = "Yhzs_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhzs_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhzss = Yhzs.createForJson(jsons);
			}else{
				yhzss = YhzsDao.getByYh_id(yh_id);
				if(yhzss!=null && !yhzss.isEmpty()){
					loadCaches(yhzss);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzss;
	}
	
	
	public static List<Yhzs> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhzs> yhzss = getByYh_id(yh_id);
		yhzss = SK_List.getPage(yhzss, page, size, pageCount);
		return yhzss;
	}
	

	public static void loadCache(Yhzs yhzs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhzs,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhzs yhzs,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhzs_Object";
		String field = SK_Plus.b("id:",yhzs.getId()).e();
		String data = yhzs.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhzs.getId());
		key = "Yhzs_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhzs.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhzs> yhzss){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhzs yhzs : yhzss){
				loadCache(yhzs,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhzs yhzs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhzs,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhzs yhzs,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhzs_Object";
		String field = SK_Plus.b("id:",yhzs.getId()).e();
		p.hdel(key,field);
		
		key = "Yhzs_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhzs.getId());
		field = SK_Plus.b(key,"yh_id:",yhzs.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhzs> yhzss){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhzs yhzs : yhzss){
				clearCache(yhzs,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhzs insert(Yhzs yhzs){
		yhzs = YhzsDao.insert(yhzs);
		if(yhzs!=null){
			loadCache(yhzs);
		}
    	return yhzs;
    }
    
    public static Yhzs update(Yhzs yhzs){
    	yhzs = YhzsDao.update(yhzs);
    	if(yhzs!=null){
    		clearCache(yhzs);
			loadCache(yhzs);
		}
    	return yhzs;
    }
    
    public static boolean delete(Yhzs yhzs){
    	boolean bool = YhzsDao.delete(yhzs);
    	if(bool){
    		clearCache(yhzs);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhzs> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		try{
			String key = "Yhzs_Object";
			List<String> jsons = jedis.hvals(key);
			yhzss = Yhzs.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhzs> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		try{
			if(!isLoadAll){
				yhzss = YhzsDao.getAll();
				loadCaches(yhzss);
				isLoadAll = true;
			}else{
				String key = "Yhzs_Object";
				List<String> jsons = jedis.hvals(key);
				yhzss = Yhzs.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhzs> getAllByPage(int page,int size,Integer pageCount){
		List<Yhzs> yhzss = getAll();
		yhzss = SK_List.getPage(yhzss, page, size, pageCount);
		return yhzss;
	}
}