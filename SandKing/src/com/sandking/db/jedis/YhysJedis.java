package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.bean.Yhys;
import java.util.Set;
import com.sandking.db.dao.YhysDao;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户映射
 */
public class YhysJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhysJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhys yhys = null;
		try{
			String key = "Yhys_Object";
			String field = SK_Plus.b("id:",id).e();
			yhys = Yhys.createForJson(jedis.hget(key,field));
			if(yhys == null){
				yhys = YhysDao.getById(id);
				if(yhys!=null){
					loadCache(yhys);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhys;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhys> yhyss = new ArrayList<Yhys>();
		try{
			String key = "Yhys_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhys_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhyss = Yhys.createForJson(jsons);
			}else{
				yhyss = YhysDao.getByYh_id(yh_id);
				if(yhyss!=null && !yhyss.isEmpty()){
					loadCaches(yhyss);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyss;
	}
	
	
	public static List<Yhys> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhys> yhyss = getByYh_id(yh_id);
		yhyss = SK_List.getPage(yhyss, page, size, pageCount);
		return yhyss;
	}
	

	public static void loadCache(Yhys yhys){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhys,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhys yhys,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhys_Object";
		String field = SK_Plus.b("id:",yhys.getId()).e();
		String data = yhys.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhys.getId());
		key = "Yhys_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhys.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhys> yhyss){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhys yhys : yhyss){
				loadCache(yhys,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhys yhys){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhys,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhys yhys,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhys_Object";
		String field = SK_Plus.b("id:",yhys.getId()).e();
		p.hdel(key,field);
		
		key = "Yhys_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhys.getId());
		field = SK_Plus.b(key,"yh_id:",yhys.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhys> yhyss){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhys yhys : yhyss){
				clearCache(yhys,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhys insert(Yhys yhys){
		yhys = YhysDao.insert(yhys);
		if(yhys!=null){
			loadCache(yhys);
		}
    	return yhys;
    }
    
    public static Yhys update(Yhys yhys){
    	yhys = YhysDao.update(yhys);
    	if(yhys!=null){
    		clearCache(yhys);
			loadCache(yhys);
		}
    	return yhys;
    }
    
    public static boolean delete(Yhys yhys){
    	boolean bool = YhysDao.delete(yhys);
    	if(bool){
    		clearCache(yhys);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhys> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhys> yhyss = new ArrayList<Yhys>();
		try{
			String key = "Yhys_Object";
			List<String> jsons = jedis.hvals(key);
			yhyss = Yhys.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhys> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhys> yhyss = new ArrayList<Yhys>();
		try{
			if(!isLoadAll){
				yhyss = YhysDao.getAll();
				loadCaches(yhyss);
				isLoadAll = true;
			}else{
				String key = "Yhys_Object";
				List<String> jsons = jedis.hvals(key);
				yhyss = Yhys.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhys> getAllByPage(int page,int size,Integer pageCount){
		List<Yhys> yhyss = getAll();
		yhyss = SK_List.getPage(yhyss, page, size, pageCount);
		return yhyss;
	}
}