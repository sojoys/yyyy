package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import com.sandking.db.dao.YhDao;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.db.bean.Yh;
import com.sandking.tools.SK_List;
/**
 * 用户
 */
public class YhJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yh yh = null;
		try{
			String key = "Yh_Object";
			String field = SK_Plus.b("id:",id).e();
			yh = Yh.createForJson(jedis.hget(key,field));
			if(yh == null){
				yh = YhDao.getById(id);
				if(yh!=null){
					loadCache(yh);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yh;
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yh yh = null;
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b("nc:",nc).e();
			String primaryKey = jedis.hget(key,field);
			if(primaryKey!=null){
				yh = getById(Integer.valueOf(primaryKey));
			}
			if(yh == null){
				yh = YhDao.getByNc(nc);
				if(yh!=null){
					loadCache(yh);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yh;
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yh yh = null;
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b("zh:",zh).e();
			String primaryKey = jedis.hget(key,field);
			if(primaryKey!=null){
				yh = getById(Integer.valueOf(primaryKey));
			}
			if(yh == null){
				yh = YhDao.getByZh(zh);
				if(yh!=null){
					loadCache(yh);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yh;
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yh yh = null;
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b("zh, mm:",zh, mm).e();
			String primaryKey = jedis.hget(key,field);
			if(primaryKey!=null){
				yh = getById(Integer.valueOf(primaryKey));
			}
			if(yh == null){
				yh = YhDao.getByZhMm(zh, mm);
				if(yh!=null){
					loadCache(yh);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yh;
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b(key,"yhlx_id:",yhlx_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yh_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhs = Yh.createForJson(jsons);
			}else{
				yhs = YhDao.getByYhlx_id(yhlx_id);
				if(yhs!=null && !yhs.isEmpty()){
					loadCaches(yhs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByYhlx_id(yhlx_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b(key,"fwq_id:",fwq_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yh_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhs = Yh.createForJson(jsons);
			}else{
				yhs = YhDao.getByFwq_id(fwq_id);
				if(yhs!=null && !yhs.isEmpty()){
					loadCaches(yhs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	
	public static List<Yh> getByPageFwq_id(int fwq_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByFwq_id(fwq_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b(key,"qd:",qd).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yh_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhs = Yh.createForJson(jsons);
			}else{
				yhs = YhDao.getByQd(qd);
				if(yhs!=null && !yhs.isEmpty()){
					loadCaches(yhs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	
	public static List<Yh> getByPageQd(String qd,int page,int size,Integer pageCount){
		List<Yh> yhs = getByQd(qd);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			String key = "Yh_Index";
			String field = SK_Plus.b(key,"lm_id:",lm_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yh_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhs = Yh.createForJson(jsons);
			}else{
				yhs = YhDao.getByLm_id(lm_id);
				if(yhs!=null && !yhs.isEmpty()){
					loadCaches(yhs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	
	public static List<Yh> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByLm_id(lm_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	

	public static void loadCache(Yh yh){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yh,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yh yh,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yh_Object";
		String field = SK_Plus.b("id:",yh.getId()).e();
		String data = yh.toJson();
		p.hset(key,field,data);
		
		// ---------------------- 唯一索引 ----------------------
		String primaryKey = String.valueOf(yh.getId());
		key = "Yh_Index";
		
		field = SK_Plus.b("nc:",yh.getNc()).e();
		p.hset(key,field,primaryKey);
		yh.setNcIndex(field);
		
		field = SK_Plus.b("zh:",yh.getZh()).e();
		p.hset(key,field,primaryKey);
		yh.setZhIndex(field);
		
		field = SK_Plus.b("zh, mm:",yh.getZh(),yh.getMm()).e();
		p.hset(key,field,primaryKey);
		yh.setZhMmIndex(field);
		
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yhlx_id:",yh.getYhlx_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"fwq_id:",yh.getFwq_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"qd:",yh.getQd()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"lm_id:",yh.getLm_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yh> yhs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yh yh : yhs){
				loadCache(yh,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yh yh){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yh,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yh yh,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yh_Object";
		String field = SK_Plus.b("id:",yh.getId()).e();
		p.hdel(key,field);
		
		// ---------------------- 唯一索引 ----------------------
		key = "Yh_Index";
		
		// field = SK_Plus.b("nc:",yh.getNc()).e();
		field = yh.getNcIndex();
		if(field!=null){
			p.hdel(key,field);
		}
		// field = SK_Plus.b("zh:",yh.getZh()).e();
		field = yh.getZhIndex();
		if(field!=null){
			p.hdel(key,field);
		}
		// field = SK_Plus.b("zh, mm:",yh.getZh(),yh.getMm()).e();
		field = yh.getZhMmIndex();
		if(field!=null){
			p.hdel(key,field);
		}
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yh.getId());
		field = SK_Plus.b(key,"yhlx_id:",yh.getYhlx_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"fwq_id:",yh.getFwq_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"qd:",yh.getQd()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"lm_id:",yh.getLm_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yh> yhs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yh yh : yhs){
				clearCache(yh,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yh insert(Yh yh){
		yh = YhDao.insert(yh);
		if(yh!=null){
			loadCache(yh);
		}
    	return yh;
    }
    
    public static Yh update(Yh yh){
    	yh = YhDao.update(yh);
    	if(yh!=null){
    		clearCache(yh);
			loadCache(yh);
		}
    	return yh;
    }
    
    public static boolean delete(Yh yh){
    	boolean bool = YhDao.delete(yh);
    	if(bool){
    		clearCache(yh);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yh> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			String key = "Yh_Object";
			List<String> jsons = jedis.hvals(key);
			yhs = Yh.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yh> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yh> yhs = new ArrayList<Yh>();
		try{
			if(!isLoadAll){
				yhs = YhDao.getAll();
				loadCaches(yhs);
				isLoadAll = true;
			}else{
				String key = "Yh_Object";
				List<String> jsons = jedis.hvals(key);
				yhs = Yh.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yh> getAllByPage(int page,int size,Integer pageCount){
		List<Yh> yhs = getAll();
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
}