package com.sandking.db;

import com.sandking.db.imp.*;

/**
 * 数据存储
 */
public class DbSave {

	public static boolean saveAll() throws Exception{
		// 兵营队列
		BydlImp.saveAll();
		
		// 建筑队列
		JzdlImp.saveAll();
		
		// 服务器
		FwqImp.saveAll();
		
		// 用户
		YhImp.saveAll();
		
		// 用户建筑
		YhjzImp.saveAll();
		
		// 用户映射
		YhysImp.saveAll();
		
		// 用户类型
		YhlxImp.saveAll();
		
		// 用户背包
		YhbbImp.saveAll();
		
		// 用户英雄
		YhyxImp.saveAll();
		
		// 用户装饰
		YhzsImp.saveAll();
		
		// 用户邮件
		YhyjImp.saveAll();
		
		// 用户陷阱
		YhxjImp.saveAll();
		
		// 用户障碍
		YhzaImp.saveAll();
		
		// 聊天
		LtImp.saveAll();
		
		// 聊天类型
		LtlxImp.saveAll();
		
		// 联盟
		LmImp.saveAll();
		
		// 英雄技能
		YxjnImp.saveAll();
		
		// 英雄状态
		YxztImp.saveAll();
		
		// 邮件类型
		YjlxImp.saveAll();
		
		// 邮件附件
		YjfjImp.saveAll();
		
		// 附件类型
		FjlxImp.saveAll();
		
		// 障碍队列
		ZadlImp.saveAll();
		
		return true;
	}
	
	public static boolean saveRedisAll() throws Exception{
		// 兵营队列
		BydlImp.saveRedis();
		
		// 建筑队列
		JzdlImp.saveRedis();
		
		// 服务器
		FwqImp.saveRedis();
		
		// 用户
		YhImp.saveRedis();
		
		// 用户建筑
		YhjzImp.saveRedis();
		
		// 用户映射
		YhysImp.saveRedis();
		
		// 用户类型
		YhlxImp.saveRedis();
		
		// 用户背包
		YhbbImp.saveRedis();
		
		// 用户英雄
		YhyxImp.saveRedis();
		
		// 用户装饰
		YhzsImp.saveRedis();
		
		// 用户邮件
		YhyjImp.saveRedis();
		
		// 用户陷阱
		YhxjImp.saveRedis();
		
		// 用户障碍
		YhzaImp.saveRedis();
		
		// 聊天
		LtImp.saveRedis();
		
		// 聊天类型
		LtlxImp.saveRedis();
		
		// 联盟
		LmImp.saveRedis();
		
		// 英雄技能
		YxjnImp.saveRedis();
		
		// 英雄状态
		YxztImp.saveRedis();
		
		// 邮件类型
		YjlxImp.saveRedis();
		
		// 邮件附件
		YjfjImp.saveRedis();
		
		// 附件类型
		FjlxImp.saveRedis();
		
		// 障碍队列
		ZadlImp.saveRedis();
		
		return true;
	}
	
	public static boolean saveDatabaseAll() throws Exception{
		// 兵营队列
		BydlImp.saveDatabase();
		
		// 建筑队列
		JzdlImp.saveDatabase();
		
		// 服务器
		FwqImp.saveDatabase();
		
		// 用户
		YhImp.saveDatabase();
		
		// 用户建筑
		YhjzImp.saveDatabase();
		
		// 用户映射
		YhysImp.saveDatabase();
		
		// 用户类型
		YhlxImp.saveDatabase();
		
		// 用户背包
		YhbbImp.saveDatabase();
		
		// 用户英雄
		YhyxImp.saveDatabase();
		
		// 用户装饰
		YhzsImp.saveDatabase();
		
		// 用户邮件
		YhyjImp.saveDatabase();
		
		// 用户陷阱
		YhxjImp.saveDatabase();
		
		// 用户障碍
		YhzaImp.saveDatabase();
		
		// 聊天
		LtImp.saveDatabase();
		
		// 聊天类型
		LtlxImp.saveDatabase();
		
		// 联盟
		LmImp.saveDatabase();
		
		// 英雄技能
		YxjnImp.saveDatabase();
		
		// 英雄状态
		YxztImp.saveDatabase();
		
		// 邮件类型
		YjlxImp.saveDatabase();
		
		// 邮件附件
		YjfjImp.saveDatabase();
		
		// 附件类型
		FjlxImp.saveDatabase();
		
		// 障碍队列
		ZadlImp.saveDatabase();
		
		return true;
	}
}