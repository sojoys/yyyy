package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import com.sandking.db.bean.Yhxj;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 用户陷阱
 */
public class YhxjDao {
	public static Yhxj insert(Yhxj yhxj){
		Connection conn = SK_Config.getConnection();
		return insert(yhxj,conn);
	}
	
	public static Yhxj insert(Yhxj yhxj,Connection conn){
		return insert(yhxj,conn,Yhxj.TABLENAME);
	}
	
	public static Yhxj insert(Yhxj yhxj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhxj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhxj insert(Yhxj yhxj,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhxj,conn,tableName);
	}
	
	public static Yhxj insert(Yhxj yhxj,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,陷阱id,x,y,用户_id) VALUES (?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhxj.getId(),yhxj.getXjid(),yhxj.getX(),yhxj.getY(),yhxj.getYh_id());
			if(yhxj.getId()==0){
				yhxj.setId(i);
			}
			return i > 0 ? yhxj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhxj insert(Yhxj yhxj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhxj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhxjs,conn);
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs,Connection conn){
		return insertBatch(yhxjs,conn,Yhxj.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhxjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhxjs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,陷阱id,x,y,用户_id) VALUES (?,?,?,?,?)";
		try {
			int columnSize = 5;
			int size = yhxjs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhxjs.get(i).getId();
				params[i][1] =yhxjs.get(i).getXjid();
				params[i][2] =yhxjs.get(i).getX();
				params[i][3] =yhxjs.get(i).getY();
				params[i][4] =yhxjs.get(i).getYh_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhxj> yhxjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhxjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhxj update(Yhxj yhxj){
		Connection conn = SK_Config.getConnection();
		return update(yhxj,conn);
	}
	
	public static Yhxj update(Yhxj yhxj,Connection conn){
		return update(yhxj,conn,Yhxj.TABLENAME);
	}
	
	public static Yhxj update(Yhxj yhxj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhxj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhxj update(Yhxj yhxj,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhxj,conn,tableName);
	}
	
	public static Yhxj update(Yhxj yhxj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhxj.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhxj;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhxj.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhxj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhxj.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhxj update(Yhxj yhxj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhxj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhxjs,conn);
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs,Connection conn){
		return updateBatch(yhxjs,conn,Yhxj.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhxjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhxjs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,陷阱id = ?,x = ?,y = ?,用户_id = ? WHERE id = ?";
		try {
			int columnSize = 5;
			int size = yhxjs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhxjs.get(i).getId();
				params[i][1] =yhxjs.get(i).getXjid();
				params[i][2] =yhxjs.get(i).getX();
				params[i][3] =yhxjs.get(i).getY();
				params[i][4] =yhxjs.get(i).getYh_id();
				params[i][columnSize] =yhxjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhxj> yhxjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhxjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhxj yhxj){
		Connection conn = SK_Config.getConnection();
		return delete(yhxj,conn);
	}
	
	public static boolean delete(Yhxj yhxj,Connection conn){
		return delete(yhxj,conn,Yhxj.TABLENAME);
	}
	
	public static boolean delete(Yhxj yhxj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhxj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhxj yhxj,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhxj,conn,tableName);
	}
	
	public static boolean delete(Yhxj yhxj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhxj.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhxj yhxj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhxj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhxj> yhxjs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhxjs,conn);
	}
	
	public static boolean deleteBatch(List<Yhxj> yhxjs,Connection conn){
		return deleteBatch(yhxjs,conn,Yhxj.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhxj> yhxjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhxjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhxj> yhxjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhxjs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhxj> yhxjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhxjs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhxjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhxj> yhxjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhxjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhxj getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhxj> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageSize);
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id,Connection conn){
		return getById(id,conn,Yhxj.TABLENAME);
	}
	
	public static Yhxj getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhxj yhxj = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhxj = Yhxj.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxj;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhxj.TABLENAME);
	}
	
	public static List<Yhxj> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC";
		List<Yhxj> yhxjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhxjs = Yhxj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhxj> getByPageYh_id(int yh_id,Connection conn,int page,int pageSize){
		return getByPageYh_id(yh_id,conn,Yhxj.TABLENAME,page,pageSize);
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhxj> yhxjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhxjs = Yhxj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxjs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhxj getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhxj> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhxj> getByPageYh_id(int yh_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhxj> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhxj> getAll(Connection conn){
		return getAll(conn,Yhxj.TABLENAME);
	}
	
	public static List<Yhxj> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhxj> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhxj> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName + " ORDER BY id ASC";
		List<Yhxj> yhxjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhxjs = Yhxj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxjs;
	}
	
	public static List<Yhxj> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhxj> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhxj> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhxj.TABLENAME,page,pageSize);
	}
	
	public static List<Yhxj> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhxj> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhxj> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhxj> yhxjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhxjs = Yhxj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxjs;
	}
	
	public static List<Yhxj> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhxj.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhxj.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhxj.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `陷阱id` INT(10) NOT NULL,");	
		plus.a("  `x` INT(10) NOT NULL,");	
		plus.a("  `y` INT(10) NOT NULL,");	
		plus.a("  `用户_id` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}