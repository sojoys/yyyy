package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yjfj;
/**
 * 邮件附件
 */
public class YjfjDao {
	public static Yjfj insert(Yjfj yjfj){
		Connection conn = SK_Config.getConnection();
		return insert(yjfj,conn);
	}
	
	public static Yjfj insert(Yjfj yjfj,Connection conn){
		return insert(yjfj,conn,Yjfj.TABLENAME);
	}
	
	public static Yjfj insert(Yjfj yjfj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yjfj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjfj insert(Yjfj yjfj,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yjfj,conn,tableName);
	}
	
	public static Yjfj insert(Yjfj yjfj,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量) VALUES (?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yjfj.getId(),yjfj.getFjid(),yjfj.getYjlx_id(),yjfj.getYhyj_id(),yjfj.getFjlx_id(),yjfj.getFjsl());
			if(yjfj.getId()==0){
				yjfj.setId(i);
			}
			return i > 0 ? yjfj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yjfj insert(Yjfj yjfj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yjfj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yjfjs,conn);
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs,Connection conn){
		return insertBatch(yjfjs,conn,Yjfj.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yjfjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yjfjs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量) VALUES (?,?,?,?,?,?)";
		try {
			int columnSize = 6;
			int size = yjfjs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yjfjs.get(i).getId();
				params[i][1] =yjfjs.get(i).getFjid();
				params[i][2] =yjfjs.get(i).getYjlx_id();
				params[i][3] =yjfjs.get(i).getYhyj_id();
				params[i][4] =yjfjs.get(i).getFjlx_id();
				params[i][5] =yjfjs.get(i).getFjsl();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yjfj> yjfjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yjfjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yjfj update(Yjfj yjfj){
		Connection conn = SK_Config.getConnection();
		return update(yjfj,conn);
	}
	
	public static Yjfj update(Yjfj yjfj,Connection conn){
		return update(yjfj,conn,Yjfj.TABLENAME);
	}
	
	public static Yjfj update(Yjfj yjfj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yjfj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjfj update(Yjfj yjfj,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yjfj,conn,tableName);
	}
	
	public static Yjfj update(Yjfj yjfj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yjfj.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yjfj;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yjfj.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yjfj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yjfj.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yjfj update(Yjfj yjfj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yjfj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yjfjs,conn);
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs,Connection conn){
		return updateBatch(yjfjs,conn,Yjfj.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yjfjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yjfjs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,附件id = ?,邮件类型_id = ?,用户邮件_id = ?,附件类型_id = ?,附件数量 = ? WHERE id = ?";
		try {
			int columnSize = 6;
			int size = yjfjs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yjfjs.get(i).getId();
				params[i][1] =yjfjs.get(i).getFjid();
				params[i][2] =yjfjs.get(i).getYjlx_id();
				params[i][3] =yjfjs.get(i).getYhyj_id();
				params[i][4] =yjfjs.get(i).getFjlx_id();
				params[i][5] =yjfjs.get(i).getFjsl();
				params[i][columnSize] =yjfjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yjfj> yjfjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yjfjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yjfj yjfj){
		Connection conn = SK_Config.getConnection();
		return delete(yjfj,conn);
	}
	
	public static boolean delete(Yjfj yjfj,Connection conn){
		return delete(yjfj,conn,Yjfj.TABLENAME);
	}
	
	public static boolean delete(Yjfj yjfj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yjfj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yjfj yjfj,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yjfj,conn,tableName);
	}
	
	public static boolean delete(Yjfj yjfj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yjfj.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yjfj yjfj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yjfj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yjfj> yjfjs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yjfjs,conn);
	}
	
	public static boolean deleteBatch(List<Yjfj> yjfjs,Connection conn){
		return deleteBatch(yjfjs,conn,Yjfj.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yjfj> yjfjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yjfjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yjfj> yjfjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yjfjs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yjfj> yjfjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yjfjs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yjfjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yjfj> yjfjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yjfjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yjfj getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id){
		Connection conn = SK_Config.getConnection();
		return getByYjlx_id(yjlx_id, conn);
	}
	
	public static List<Yjfj> getByYjlx_id(int yjlx_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYjlx_id(yjlx_id, conn,tableName);
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYjlx_id(yjlx_id, conn,page,pageSize);
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYjlx_id(yjlx_id, conn,tableName,page,pageSize);
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id){
		Connection conn = SK_Config.getConnection();
		return getByYhyj_id(yhyj_id, conn);
	}
	
	public static List<Yjfj> getByYhyj_id(int yhyj_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhyj_id(yhyj_id, conn,tableName);
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYhyj_id(yhyj_id, conn,page,pageSize);
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYhyj_id(yhyj_id, conn,tableName,page,pageSize);
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id){
		Connection conn = SK_Config.getConnection();
		return getByFjlx_id(fjlx_id, conn);
	}
	
	public static List<Yjfj> getByFjlx_id(int fjlx_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByFjlx_id(fjlx_id, conn,tableName);
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageFjlx_id(fjlx_id, conn,page,pageSize);
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageFjlx_id(fjlx_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id,Connection conn){
		return getById(id,conn,Yjfj.TABLENAME);
	}
	
	public static Yjfj getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yjfj yjfj = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yjfj = Yjfj.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfj;
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id,Connection conn){
		return getByYjlx_id(yjlx_id,conn,Yjfj.TABLENAME);
	}
	
	public static List<Yjfj> getByYjlx_id(int yjlx_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "yjlx_id = ? ORDER BY id ASC";
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yjlx_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,Connection conn,int page,int pageSize){
		return getByPageYjlx_id(yjlx_id,conn,Yjfj.TABLENAME,page,pageSize);
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "yjlx_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yjlx_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id,Connection conn){
		return getByYhyj_id(yhyj_id,conn,Yjfj.TABLENAME);
	}
	
	public static List<Yjfj> getByYhyj_id(int yhyj_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "yhyj_id = ? ORDER BY id ASC";
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhyj_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,Connection conn,int page,int pageSize){
		return getByPageYhyj_id(yhyj_id,conn,Yjfj.TABLENAME,page,pageSize);
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "yhyj_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhyj_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id,Connection conn){
		return getByFjlx_id(fjlx_id,conn,Yjfj.TABLENAME);
	}
	
	public static List<Yjfj> getByFjlx_id(int fjlx_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "fjlx_id = ? ORDER BY id ASC";
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fjlx_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,Connection conn,int page,int pageSize){
		return getByPageFjlx_id(fjlx_id,conn,Yjfj.TABLENAME,page,pageSize);
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " WHERE " + "fjlx_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fjlx_id);
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjfj getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYjlx_id(yjlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByYjlx_id(int yjlx_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYjlx_id(yjlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYjlx_id(yjlx_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYjlx_id(yjlx_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhyj_id(yhyj_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByYhyj_id(int yhyj_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhyj_id(yhyj_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhyj_id(yhyj_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhyj_id(yhyj_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByFjlx_id(fjlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByFjlx_id(int fjlx_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByFjlx_id(fjlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageFjlx_id(fjlx_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageFjlx_id(fjlx_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yjfj> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yjfj> getAll(Connection conn){
		return getAll(conn,Yjfj.TABLENAME);
	}
	
	public static List<Yjfj> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yjfj> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " ORDER BY id ASC";
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yjfj> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yjfj> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yjfj.TABLENAME,page,pageSize);
	}
	
	public static List<Yjfj> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjfj> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yjfj> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yjfj> yjfjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yjfjs = Yjfj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yjfj.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yjfj.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yjfj.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `附件id` INT(10) NOT NULL,");	
		plus.a("  `邮件类型_id` INT(10) NOT NULL,");	
		plus.a("  `用户邮件_id` INT(10) NOT NULL,");	
		plus.a("  `附件类型_id` INT(10) NOT NULL,");	
		plus.a("  `附件数量` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}