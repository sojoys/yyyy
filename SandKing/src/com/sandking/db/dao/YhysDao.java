package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yhys;
/**
 * 用户映射
 */
public class YhysDao {
	public static Yhys insert(Yhys yhys){
		Connection conn = SK_Config.getConnection();
		return insert(yhys,conn);
	}
	
	public static Yhys insert(Yhys yhys,Connection conn){
		return insert(yhys,conn,Yhys.TABLENAME);
	}
	
	public static Yhys insert(Yhys yhys,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhys,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhys insert(Yhys yhys,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhys,conn,tableName);
	}
	
	public static Yhys insert(Yhys yhys,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,建筑,障碍,装饰,陷阱,用户_id) VALUES (?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhys.getId(),yhys.getJz(),yhys.getZa(),yhys.getZs(),yhys.getXj(),yhys.getYh_id());
			if(yhys.getId()==0){
				yhys.setId(i);
			}
			return i > 0 ? yhys : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhys insert(Yhys yhys,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhys,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhys> yhyss){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyss,conn);
	}
	
	public static int[] insertBatch(List<Yhys> yhyss,Connection conn){
		return insertBatch(yhyss,conn,Yhys.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhys> yhyss,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyss,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhys> yhyss,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyss,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhys> yhyss,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,建筑,障碍,装饰,陷阱,用户_id) VALUES (?,?,?,?,?,?)";
		try {
			int columnSize = 6;
			int size = yhyss.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyss.get(i).getId();
				params[i][1] =yhyss.get(i).getJz();
				params[i][2] =yhyss.get(i).getZa();
				params[i][3] =yhyss.get(i).getZs();
				params[i][4] =yhyss.get(i).getXj();
				params[i][5] =yhyss.get(i).getYh_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhys> yhyss,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyss,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhys update(Yhys yhys){
		Connection conn = SK_Config.getConnection();
		return update(yhys,conn);
	}
	
	public static Yhys update(Yhys yhys,Connection conn){
		return update(yhys,conn,Yhys.TABLENAME);
	}
	
	public static Yhys update(Yhys yhys,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhys,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhys update(Yhys yhys,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhys,conn,tableName);
	}
	
	public static Yhys update(Yhys yhys,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhys.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhys;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhys.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhys : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhys.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhys update(Yhys yhys,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhys,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhys> yhyss){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyss,conn);
	}
	
	public static int[] updateBatch(List<Yhys> yhyss,Connection conn){
		return updateBatch(yhyss,conn,Yhys.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhys> yhyss,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyss,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhys> yhyss,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyss,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhys> yhyss,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,建筑 = ?,障碍 = ?,装饰 = ?,陷阱 = ?,用户_id = ? WHERE id = ?";
		try {
			int columnSize = 6;
			int size = yhyss.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyss.get(i).getId();
				params[i][1] =yhyss.get(i).getJz();
				params[i][2] =yhyss.get(i).getZa();
				params[i][3] =yhyss.get(i).getZs();
				params[i][4] =yhyss.get(i).getXj();
				params[i][5] =yhyss.get(i).getYh_id();
				params[i][columnSize] =yhyss.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhys> yhyss,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyss,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhys yhys){
		Connection conn = SK_Config.getConnection();
		return delete(yhys,conn);
	}
	
	public static boolean delete(Yhys yhys,Connection conn){
		return delete(yhys,conn,Yhys.TABLENAME);
	}
	
	public static boolean delete(Yhys yhys,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhys,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhys yhys,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhys,conn,tableName);
	}
	
	public static boolean delete(Yhys yhys,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhys.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhys yhys,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhys,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhys> yhyss){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyss,conn);
	}
	
	public static boolean deleteBatch(List<Yhys> yhyss,Connection conn){
		return deleteBatch(yhyss,conn,Yhys.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhys> yhyss,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyss,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhys> yhyss,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyss,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhys> yhyss,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhyss.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhyss.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhys> yhyss,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyss,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhys getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhys> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageSize);
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id,Connection conn){
		return getById(id,conn,Yhys.TABLENAME);
	}
	
	public static Yhys getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhys yhys = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhys = Yhys.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhys;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhys.TABLENAME);
	}
	
	public static List<Yhys> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC";
		List<Yhys> yhyss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhyss = Yhys.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyss;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhys> getByPageYh_id(int yh_id,Connection conn,int page,int pageSize){
		return getByPageYh_id(yh_id,conn,Yhys.TABLENAME,page,pageSize);
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhys> yhyss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhyss = Yhys.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyss;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhys getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhys> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhys> getByPageYh_id(int yh_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhys> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhys> getAll(Connection conn){
		return getAll(conn,Yhys.TABLENAME);
	}
	
	public static List<Yhys> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhys> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhys> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName + " ORDER BY id ASC";
		List<Yhys> yhyss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyss = Yhys.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyss;
	}
	
	public static List<Yhys> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhys> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhys> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhys.TABLENAME,page,pageSize);
	}
	
	public static List<Yhys> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhys> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhys> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhys> yhyss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyss = Yhys.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyss;
	}
	
	public static List<Yhys> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhys.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhys.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhys.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `建筑` VARCHAR(45) NOT NULL,");	
		plus.a("  `障碍` VARCHAR(45) NOT NULL,");	
		plus.a("  `装饰` VARCHAR(45) NOT NULL,");	
		plus.a("  `陷阱` VARCHAR(45) NOT NULL,");	
		plus.a("  `用户_id` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}