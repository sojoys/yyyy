package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import com.sandking.db.cache.YjlxCache;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YjfjCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 邮件类型
 */
public class Yjlx {

	public static final String TABLENAME = "邮件类型";
	public static final String CLASSNAME = "Yjlx"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public Yjlx() {
		super();
	}
	
	public Yjlx(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	
	//id
	public List<Yjfj> getYjfjsFkYjlx_id(){
		return YjfjCache.getByYjlx_id(id);
	}
	
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Yjlx createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yjlx yjlx = new Yjlx();
		    yjlx.id = SK_InputStream.readInt(in,null);
		    yjlx.mc = SK_InputStream.readString(in,null);
		    return yjlx;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Yjlx createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Yjlx yjlx = Yjlx.createForMap(map);
		    return yjlx;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yjlx> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yjlx> yjlxs = new ArrayList<Yjlx>();
		for (Map<String, Object> map : list) {
			yjlxs.add(createForColumnNameMap(map));
		}
		return yjlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yjlx createForColumnNameMap(Map<String, Object> map){
    	Yjlx obj = new Yjlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yjlx> createForList(List<Map<String, Object>> list){
    	List<Yjlx> yjlxs = new ArrayList<Yjlx>();
		for (Map<String, Object> map : list) {
			yjlxs.add(createForColumnNameMap(map));
		}
		return yjlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yjlx createForMap(Map<String, Object> map){
    	Yjlx obj = new Yjlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
        return obj;
    }
    
    public static Yjlx createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Yjlx> createForJson(List<String> jsons){
    	List<Yjlx> yjlxs = new ArrayList<Yjlx>();
    	for(String json : jsons){
    		yjlxs.add(createForJson(json));
    	}
    	return yjlxs;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Yjlx insert(){
    	return YjlxCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yjlx update(){
    	return YjlxCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YjlxCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yjlx insertAndFlush(){
    	return YjlxCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yjlx updateAndFlush(){
    	return YjlxCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YjlxCache.deleteAndFlush(this);
    }
}