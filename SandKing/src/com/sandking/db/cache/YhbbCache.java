package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.YhbbJedis;
import com.sandking.db.dao.YhbbDao;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Yhbb;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 用户背包
 */
public class YhbbCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhbbCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhbb> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhbb> saveDelete = new CopyOnWriteArrayList<Yhbb>();
	
	static final List<Yhbb> saveInsert = new CopyOnWriteArrayList<Yhbb>();
	
	static final List<Yhbb> saveUpdate = new CopyOnWriteArrayList<Yhbb>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id){
		Yhbb yhbb = null;
		String key = SK_Plus.b(id).e();
		yhbb = idCache.get(key);
		
		if(yhbb==null){
			//查询数据库
			yhbb = YhbbJedis.getById(id);
			if(yhbb!=null){
				//加入缓存
				loadCache(yhbb);
			}
		}
		return yhbb;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id){
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhbb yhbb = null;
			for (String k : keys) {
				yhbb = getById(Integer.valueOf(k));
				if (yhbb == null) continue;
					yhbbs.add(yhbb);
			}
		}else{
			yhbbs = YhbbJedis.getByYh_id(yh_id);
			if(!yhbbs.isEmpty()){
				loadCaches(yhbbs);
			}
		}
		return yhbbs;
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhbb> yhbbs = getByYh_id(yh_id);
		yhbbs = SK_List.getPage(yhbbs, page, size, pageCount);
		return yhbbs;
	}
	
	public static List<Yhbb> getCacheAll(){
		return new ArrayList<Yhbb>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhbb> getAll(){
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		if(!isLoadAll){
			yhbbs = YhbbJedis.getAll();
			loadCaches(yhbbs);
			isLoadAll = true;
		}else{
			yhbbs = new ArrayList<Yhbb>(idCache.values());
		}
		return yhbbs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhbb> getAllByPage(int page,int size,Integer pageCount){
		List<Yhbb> yhbbs = getAll();
		yhbbs = SK_List.getPage(yhbbs, page, size, pageCount);
		return yhbbs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhbb> yhbbs){
		for(Yhbb yhbb : yhbbs){
			loadCache(yhbb);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhbb yhbb){
		idCache.put(SK_Plus.b(yhbb.getId()).e(),yhbb);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhbb.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhbb.getId()));
		yh_idCache.put(SK_Plus.b(yhbb.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhbb yhbb,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhbb.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhbb.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhbb.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhbb.getId()))) {
			yh_idset.remove(String.valueOf(yhbb.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhbb.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhbb);
			break;
		case 2:
			saveRedis(yhbb);
			break;
		case 3:
			saveAll(yhbb);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhbb> yhbbs,int clearType,int saveType){
		for(Yhbb yhbb : yhbbs){
			clearCache(yhbb,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhbbs);
			break;
		case 2:
			saveRedis(yhbbs);
			break;
		case 3:
			saveAll(yhbbs);
			break;
		default:
			break;
		}
	}
	
	public static Yhbb insert(Yhbb yhbb){
		return insert(yhbb,false);
    }
    
    public static Yhbb update(Yhbb yhbb){
    	return update(yhbb,false);
    }
    
    public static boolean delete(Yhbb yhbb){
    	return delete(yhbb,false);
    }
    
    private static Yhbb insert(Yhbb yhbb,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhbb = YhbbJedis.insert(yhbb);
    		LASTID.set(yhbb.getId());
    	}else{
    		int _id = yhbb.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhbb.getId() > id) {
				LASTID.set(yhbb.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhbb);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhbb)) {
				saveInsert.add(yhbb);
			}
		}
    	return yhbb;
    }
    
    private static Yhbb update(Yhbb yhbb,boolean isFlush){
    	clearCache(yhbb,1,0);
    	loadCache(yhbb);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhbb)) {
				saveUpdate.add(yhbb);
			}
		}else{
			saveUpdate.remove(yhbb);
		}
    	return yhbb;
    }
    
    private static boolean delete(Yhbb yhbb,boolean isFlush){
    	clearCache(yhbb,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhbb)) {
				saveUpdate.remove(yhbb);
				saveInsert.remove(yhbb);
				saveDelete.add(yhbb);
			}
    	}else{
    		saveUpdate.remove(yhbb);
			saveInsert.remove(yhbb);
			saveDelete.remove(yhbb);
    	}
    	return false;
    }
    
    public static Yhbb updateAndFlush(Yhbb yhbb){
    	update(yhbb,true);
    	return YhbbJedis.update(yhbb);
    }
    
    public static Yhbb insertAndFlush(Yhbb yhbb){
    	int id = LASTID.get();
    	insert(yhbb,true);
    	if(id > 0){
    		yhbb = YhbbJedis.insert(yhbb);
    	}
    	return yhbb;
    }
    
    public static boolean deleteAndFlush(Yhbb yhbb){
    	delete(yhbb,true);
    	return YhbbJedis.delete(yhbb);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhbbDao.deleteBatch(saveDelete);
    	YhbbDao.insertBatch(saveInsert);
    	YhbbDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhbb yhbb){
    	if (saveDelete.remove(yhbb))
			YhbbDao.delete(yhbb);
		if (saveInsert.remove(yhbb))
			YhbbDao.insert(yhbb);
		if (saveUpdate.remove(yhbb))
			YhbbDao.update(yhbb);
    }
    
    public static void saveDatabase(List<Yhbb> yhbbs){
    	if (saveDelete.removeAll(yhbbs))
			YhbbDao.deleteBatch(yhbbs);
		if (saveInsert.removeAll(yhbbs))
			YhbbDao.insertBatch(yhbbs);
		if (saveUpdate.removeAll(yhbbs))
			YhbbDao.updateBatch(yhbbs);
    }
    
    public static void saveRedis(){
    	YhbbJedis.clearCaches(saveDelete);
    	YhbbJedis.loadCaches(saveInsert);
    	YhbbJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhbb yhbb){
    	if (saveDelete.remove(yhbb))
			YhbbJedis.clearCache(yhbb);
		if (saveInsert.remove(yhbb))
			YhbbJedis.loadCache(yhbb);
		if (saveUpdate.remove(yhbb))
			YhbbJedis.loadCache(yhbb);
    }
    
    public static void saveRedis(List<Yhbb> yhbbs){
    	if (saveDelete.removeAll(yhbbs))
			YhbbJedis.clearCaches(yhbbs);
		if (saveInsert.removeAll(yhbbs))
			YhbbJedis.loadCaches(yhbbs);
		if (saveUpdate.removeAll(yhbbs))
			YhbbJedis.loadCaches(yhbbs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhbb yhbb){
   		saveDatabase(yhbb);
   		saveRedis(yhbb);
    }
    
    public static void saveAll(List<Yhbb> yhbbs){
   		saveDatabase(yhbbs);
   		saveRedis(yhbbs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhbb yhbb){
	
		loadCache(yhbb);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhbb> yhbbs){
		for(Yhbb yhbb : yhbbs){
			loadCacheCascade(yhbb);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhbb yhbb,int saveType){
	
		clearCache(yhbb,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhbb> yhbbs,int saveType){
		for(Yhbb yhbb : yhbbs){
			clearCacheCascade(yhbb,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhbbs);
			break;
		case 2:
			saveRedis(yhbbs);
			break;
		case 3:
			saveAll(yhbbs);
			break;
		default:
			break;
		}
	}
}