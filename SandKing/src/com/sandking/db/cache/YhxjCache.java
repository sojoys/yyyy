package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.YhxjJedis;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.bean.Yhxj;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.dao.YhxjDao;
import java.util.Set;
/**
 * 用户陷阱
 */
public class YhxjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhxjCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhxj> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhxj> saveDelete = new CopyOnWriteArrayList<Yhxj>();
	
	static final List<Yhxj> saveInsert = new CopyOnWriteArrayList<Yhxj>();
	
	static final List<Yhxj> saveUpdate = new CopyOnWriteArrayList<Yhxj>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id){
		Yhxj yhxj = null;
		String key = SK_Plus.b(id).e();
		yhxj = idCache.get(key);
		
		if(yhxj==null){
			//查询数据库
			yhxj = YhxjJedis.getById(id);
			if(yhxj!=null){
				//加入缓存
				loadCache(yhxj);
			}
		}
		return yhxj;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id){
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhxj yhxj = null;
			for (String k : keys) {
				yhxj = getById(Integer.valueOf(k));
				if (yhxj == null) continue;
					yhxjs.add(yhxj);
			}
		}else{
			yhxjs = YhxjJedis.getByYh_id(yh_id);
			if(!yhxjs.isEmpty()){
				loadCaches(yhxjs);
			}
		}
		return yhxjs;
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhxj> yhxjs = getByYh_id(yh_id);
		yhxjs = SK_List.getPage(yhxjs, page, size, pageCount);
		return yhxjs;
	}
	
	public static List<Yhxj> getCacheAll(){
		return new ArrayList<Yhxj>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhxj> getAll(){
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		if(!isLoadAll){
			yhxjs = YhxjJedis.getAll();
			loadCaches(yhxjs);
			isLoadAll = true;
		}else{
			yhxjs = new ArrayList<Yhxj>(idCache.values());
		}
		return yhxjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhxj> getAllByPage(int page,int size,Integer pageCount){
		List<Yhxj> yhxjs = getAll();
		yhxjs = SK_List.getPage(yhxjs, page, size, pageCount);
		return yhxjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhxj> yhxjs){
		for(Yhxj yhxj : yhxjs){
			loadCache(yhxj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhxj yhxj){
		idCache.put(SK_Plus.b(yhxj.getId()).e(),yhxj);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhxj.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhxj.getId()));
		yh_idCache.put(SK_Plus.b(yhxj.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhxj yhxj,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhxj.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhxj.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhxj.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhxj.getId()))) {
			yh_idset.remove(String.valueOf(yhxj.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhxj.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhxj);
			break;
		case 2:
			saveRedis(yhxj);
			break;
		case 3:
			saveAll(yhxj);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhxj> yhxjs,int clearType,int saveType){
		for(Yhxj yhxj : yhxjs){
			clearCache(yhxj,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhxjs);
			break;
		case 2:
			saveRedis(yhxjs);
			break;
		case 3:
			saveAll(yhxjs);
			break;
		default:
			break;
		}
	}
	
	public static Yhxj insert(Yhxj yhxj){
		return insert(yhxj,false);
    }
    
    public static Yhxj update(Yhxj yhxj){
    	return update(yhxj,false);
    }
    
    public static boolean delete(Yhxj yhxj){
    	return delete(yhxj,false);
    }
    
    private static Yhxj insert(Yhxj yhxj,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhxj = YhxjJedis.insert(yhxj);
    		LASTID.set(yhxj.getId());
    	}else{
    		int _id = yhxj.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhxj.getId() > id) {
				LASTID.set(yhxj.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhxj);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhxj)) {
				saveInsert.add(yhxj);
			}
		}
    	return yhxj;
    }
    
    private static Yhxj update(Yhxj yhxj,boolean isFlush){
    	clearCache(yhxj,1,0);
    	loadCache(yhxj);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhxj)) {
				saveUpdate.add(yhxj);
			}
		}else{
			saveUpdate.remove(yhxj);
		}
    	return yhxj;
    }
    
    private static boolean delete(Yhxj yhxj,boolean isFlush){
    	clearCache(yhxj,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhxj)) {
				saveUpdate.remove(yhxj);
				saveInsert.remove(yhxj);
				saveDelete.add(yhxj);
			}
    	}else{
    		saveUpdate.remove(yhxj);
			saveInsert.remove(yhxj);
			saveDelete.remove(yhxj);
    	}
    	return false;
    }
    
    public static Yhxj updateAndFlush(Yhxj yhxj){
    	update(yhxj,true);
    	return YhxjJedis.update(yhxj);
    }
    
    public static Yhxj insertAndFlush(Yhxj yhxj){
    	int id = LASTID.get();
    	insert(yhxj,true);
    	if(id > 0){
    		yhxj = YhxjJedis.insert(yhxj);
    	}
    	return yhxj;
    }
    
    public static boolean deleteAndFlush(Yhxj yhxj){
    	delete(yhxj,true);
    	return YhxjJedis.delete(yhxj);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhxjDao.deleteBatch(saveDelete);
    	YhxjDao.insertBatch(saveInsert);
    	YhxjDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhxj yhxj){
    	if (saveDelete.remove(yhxj))
			YhxjDao.delete(yhxj);
		if (saveInsert.remove(yhxj))
			YhxjDao.insert(yhxj);
		if (saveUpdate.remove(yhxj))
			YhxjDao.update(yhxj);
    }
    
    public static void saveDatabase(List<Yhxj> yhxjs){
    	if (saveDelete.removeAll(yhxjs))
			YhxjDao.deleteBatch(yhxjs);
		if (saveInsert.removeAll(yhxjs))
			YhxjDao.insertBatch(yhxjs);
		if (saveUpdate.removeAll(yhxjs))
			YhxjDao.updateBatch(yhxjs);
    }
    
    public static void saveRedis(){
    	YhxjJedis.clearCaches(saveDelete);
    	YhxjJedis.loadCaches(saveInsert);
    	YhxjJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhxj yhxj){
    	if (saveDelete.remove(yhxj))
			YhxjJedis.clearCache(yhxj);
		if (saveInsert.remove(yhxj))
			YhxjJedis.loadCache(yhxj);
		if (saveUpdate.remove(yhxj))
			YhxjJedis.loadCache(yhxj);
    }
    
    public static void saveRedis(List<Yhxj> yhxjs){
    	if (saveDelete.removeAll(yhxjs))
			YhxjJedis.clearCaches(yhxjs);
		if (saveInsert.removeAll(yhxjs))
			YhxjJedis.loadCaches(yhxjs);
		if (saveUpdate.removeAll(yhxjs))
			YhxjJedis.loadCaches(yhxjs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhxj yhxj){
   		saveDatabase(yhxj);
   		saveRedis(yhxj);
    }
    
    public static void saveAll(List<Yhxj> yhxjs){
   		saveDatabase(yhxjs);
   		saveRedis(yhxjs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhxj yhxj){
	
		loadCache(yhxj);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhxj> yhxjs){
		for(Yhxj yhxj : yhxjs){
			loadCacheCascade(yhxj);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhxj yhxj,int saveType){
	
		clearCache(yhxj,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhxj> yhxjs,int saveType){
		for(Yhxj yhxj : yhxjs){
			clearCacheCascade(yhxj,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhxjs);
			break;
		case 2:
			saveRedis(yhxjs);
			break;
		case 3:
			saveAll(yhxjs);
			break;
		default:
			break;
		}
	}
}