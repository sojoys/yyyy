package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Zadl;
import java.util.List;
import com.sandking.db.jedis.ZadlJedis;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import com.sandking.db.dao.ZadlDao;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 障碍队列
 */
public class ZadlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		ZadlCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Zadl> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhza_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Zadl> saveDelete = new CopyOnWriteArrayList<Zadl>();
	
	static final List<Zadl> saveInsert = new CopyOnWriteArrayList<Zadl>();
	
	static final List<Zadl> saveUpdate = new CopyOnWriteArrayList<Zadl>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id){
		Zadl zadl = null;
		String key = SK_Plus.b(id).e();
		zadl = idCache.get(key);
		
		if(zadl==null){
			//查询数据库
			zadl = ZadlJedis.getById(id);
			if(zadl!=null){
				//加入缓存
				loadCache(zadl);
			}
		}
		return zadl;
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id){
		List<Zadl> zadls = new ArrayList<Zadl>();
		String key = SK_Plus.b(yhza_id).e();
		Set<String> keys = yhza_idCache.get(key);
		if(keys != null){
			Zadl zadl = null;
			for (String k : keys) {
				zadl = getById(Integer.valueOf(k));
				if (zadl == null) continue;
					zadls.add(zadl);
			}
		}else{
			zadls = ZadlJedis.getByYhza_id(yhza_id);
			if(!zadls.isEmpty()){
				loadCaches(zadls);
			}
		}
		return zadls;
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,int page,int size,Integer pageCount){
		List<Zadl> zadls = getByYhza_id(yhza_id);
		zadls = SK_List.getPage(zadls, page, size, pageCount);
		return zadls;
	}
	
	public static List<Zadl> getCacheAll(){
		return new ArrayList<Zadl>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Zadl> getAll(){
		List<Zadl> zadls = new ArrayList<Zadl>();
		if(!isLoadAll){
			zadls = ZadlJedis.getAll();
			loadCaches(zadls);
			isLoadAll = true;
		}else{
			zadls = new ArrayList<Zadl>(idCache.values());
		}
		return zadls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Zadl> getAllByPage(int page,int size,Integer pageCount){
		List<Zadl> zadls = getAll();
		zadls = SK_List.getPage(zadls, page, size, pageCount);
		return zadls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Zadl> zadls){
		for(Zadl zadl : zadls){
			loadCache(zadl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Zadl zadl){
		idCache.put(SK_Plus.b(zadl.getId()).e(),zadl);
		Set<String> yhza_idset = yhza_idCache.get(String.valueOf(SK_Plus.b(zadl.getYhza_id()).e()));
		if(yhza_idset == null){
			yhza_idset = new HashSet<String>();
		}
		yhza_idset.add(String.valueOf(zadl.getId()));
		yhza_idCache.put(SK_Plus.b(zadl.getYhza_id()).e(),yhza_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Zadl zadl,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(zadl.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(zadl.getId()).e());
		}
		Set<String> yhza_idset = yhza_idCache.get(String.valueOf(SK_Plus.b(zadl.getYhza_id()).e()));
		if(yhza_idset == null){
			yhza_idset = new HashSet<String>();
		}
		if (yhza_idset.contains(String.valueOf(zadl.getId()))) {
			yhza_idset.remove(String.valueOf(zadl.getId()));
		}
		yhza_idCache.put(SK_Plus.b(zadl.getYhza_id()).e(),yhza_idset);
		switch (saveType) {
		case 1:
			saveDatabase(zadl);
			break;
		case 2:
			saveRedis(zadl);
			break;
		case 3:
			saveAll(zadl);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Zadl> zadls,int clearType,int saveType){
		for(Zadl zadl : zadls){
			clearCache(zadl,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(zadls);
			break;
		case 2:
			saveRedis(zadls);
			break;
		case 3:
			saveAll(zadls);
			break;
		default:
			break;
		}
	}
	
	public static Zadl insert(Zadl zadl){
		return insert(zadl,false);
    }
    
    public static Zadl update(Zadl zadl){
    	return update(zadl,false);
    }
    
    public static boolean delete(Zadl zadl){
    	return delete(zadl,false);
    }
    
    private static Zadl insert(Zadl zadl,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		zadl = ZadlJedis.insert(zadl);
    		LASTID.set(zadl.getId());
    	}else{
    		int _id = zadl.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (zadl.getId() > id) {
				LASTID.set(zadl.getId());
			}
    		//加入定时器
    	}
    	loadCache(zadl);
    	if(!isFlush){
	    	if (!saveInsert.contains(zadl)) {
				saveInsert.add(zadl);
			}
		}
    	return zadl;
    }
    
    private static Zadl update(Zadl zadl,boolean isFlush){
    	clearCache(zadl,1,0);
    	loadCache(zadl);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(zadl)) {
				saveUpdate.add(zadl);
			}
		}else{
			saveUpdate.remove(zadl);
		}
    	return zadl;
    }
    
    private static boolean delete(Zadl zadl,boolean isFlush){
    	clearCache(zadl,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(zadl)) {
				saveUpdate.remove(zadl);
				saveInsert.remove(zadl);
				saveDelete.add(zadl);
			}
    	}else{
    		saveUpdate.remove(zadl);
			saveInsert.remove(zadl);
			saveDelete.remove(zadl);
    	}
    	return false;
    }
    
    public static Zadl updateAndFlush(Zadl zadl){
    	update(zadl,true);
    	return ZadlJedis.update(zadl);
    }
    
    public static Zadl insertAndFlush(Zadl zadl){
    	int id = LASTID.get();
    	insert(zadl,true);
    	if(id > 0){
    		zadl = ZadlJedis.insert(zadl);
    	}
    	return zadl;
    }
    
    public static boolean deleteAndFlush(Zadl zadl){
    	delete(zadl,true);
    	return ZadlJedis.delete(zadl);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	ZadlDao.deleteBatch(saveDelete);
    	ZadlDao.insertBatch(saveInsert);
    	ZadlDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Zadl zadl){
    	if (saveDelete.remove(zadl))
			ZadlDao.delete(zadl);
		if (saveInsert.remove(zadl))
			ZadlDao.insert(zadl);
		if (saveUpdate.remove(zadl))
			ZadlDao.update(zadl);
    }
    
    public static void saveDatabase(List<Zadl> zadls){
    	if (saveDelete.removeAll(zadls))
			ZadlDao.deleteBatch(zadls);
		if (saveInsert.removeAll(zadls))
			ZadlDao.insertBatch(zadls);
		if (saveUpdate.removeAll(zadls))
			ZadlDao.updateBatch(zadls);
    }
    
    public static void saveRedis(){
    	ZadlJedis.clearCaches(saveDelete);
    	ZadlJedis.loadCaches(saveInsert);
    	ZadlJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Zadl zadl){
    	if (saveDelete.remove(zadl))
			ZadlJedis.clearCache(zadl);
		if (saveInsert.remove(zadl))
			ZadlJedis.loadCache(zadl);
		if (saveUpdate.remove(zadl))
			ZadlJedis.loadCache(zadl);
    }
    
    public static void saveRedis(List<Zadl> zadls){
    	if (saveDelete.removeAll(zadls))
			ZadlJedis.clearCaches(zadls);
		if (saveInsert.removeAll(zadls))
			ZadlJedis.loadCaches(zadls);
		if (saveUpdate.removeAll(zadls))
			ZadlJedis.loadCaches(zadls);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Zadl zadl){
   		saveDatabase(zadl);
   		saveRedis(zadl);
    }
    
    public static void saveAll(List<Zadl> zadls){
   		saveDatabase(zadls);
   		saveRedis(zadls);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Zadl zadl){
	
		loadCache(zadl);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Zadl> zadls){
		for(Zadl zadl : zadls){
			loadCacheCascade(zadl);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Zadl zadl,int saveType){
	
		clearCache(zadl,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Zadl> zadls,int saveType){
		for(Zadl zadl : zadls){
			clearCacheCascade(zadl,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(zadls);
			break;
		case 2:
			saveRedis(zadls);
			break;
		case 3:
			saveAll(zadls);
			break;
		default:
			break;
		}
	}
}