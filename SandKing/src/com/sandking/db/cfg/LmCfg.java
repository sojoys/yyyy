package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 联盟
 */
public class LmCfg {

	public static final String TABLENAME = "联盟";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LmCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, LmCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> cjridCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	/** 创建人id */
	private int cjrid;
	
	
	public LmCfg() {
		super();
	}
	
	public LmCfg(int id, String mc, int cjrid) {
		super();
		this.id = id;
		this.mc = mc;
		this.cjrid = cjrid;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	public int getCjrid() {
		return cjrid;
	}
	
	public void setCjrid(int cjrid) {
		this.cjrid = cjrid;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<LmCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<LmCfg> lmCfgs = new ArrayList<LmCfg>();
		for (Map<String, Object> map : list) {
			lmCfgs.add(createForColumnNameMap(map));
		}
		return lmCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static LmCfg createForColumnNameMap(Map<String, Object> map){
    	LmCfg obj = new LmCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
	    obj.cjrid = SK_Map.getInt("创建人id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
	    SK_OutputStream.writeInt(out,this.cjrid);
    }
    
     public static LmCfg forStream(ByteArrayInputStream in) throws Exception {
     	LmCfg lmCfg = new LmCfg();
	    lmCfg.id = SK_InputStream.readInt(in,null);
	    lmCfg.mc = SK_InputStream.readString(in,null);
	    lmCfg.cjrid = SK_InputStream.readInt(in,null);
	    return lmCfg;
     }
    
    public static List<LmCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<LmCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,LmCfg.TABLENAME);
	}
	
	public static List<LmCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<LmCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<LmCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,创建人id FROM " + tableName;
		List<LmCfg> lmCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			lmCfgs = LmCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lmCfgs;
	}
	
	public static List<LmCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<LmCfg> lmCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, lmCfgs.size());
			for(LmCfg lmCfg : lmCfgs){
				lmCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(LmCfg lmCfg){
		idCache.put(SK_Plus.b(lmCfg.getId()).e(),lmCfg);
		Set<String> cjridset = cjridCache.get(String.valueOf(SK_Plus.b(lmCfg.getCjrid()).e()));
		if(cjridset == null){
			cjridset = new HashSet<String>();
		}
		cjridset.add(String.valueOf(lmCfg.getId()));
		cjridCache.put(SK_Plus.b(lmCfg.getCjrid()).e(),cjridset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static LmCfg getById(int id){
		LmCfg lmCfg = null;
		String key = SK_Plus.b(id).e();
		lmCfg = idCache.get(key);
		
		return lmCfg;
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<LmCfg> getByCjrid(int cjrid){
		List<LmCfg> lmCfgs = new ArrayList<LmCfg>();
		String key = SK_Plus.b(cjrid).e();
		Set<String> keys = cjridCache.get(key);
		if(keys != null){
			LmCfg lmCfg = null;
			for (String k : keys) {
				lmCfg = getById(Integer.valueOf(k));
				if (lmCfg == null) continue;
					lmCfgs.add(lmCfg);
			}
		}
		return lmCfgs;
	}
	
	public static List<LmCfg> getByPageCjrid(int cjrid,int page,int size,Integer pageCount){
		List<LmCfg> lmCfgs = getByCjrid(cjrid);
		lmCfgs = SK_List.getPage(lmCfgs, page, size, pageCount);
		return lmCfgs;
	}
	
	public static List<LmCfg> getAll(){
		return new ArrayList<LmCfg>(idCache.values());
	}
	
	public static List<LmCfg> getAll(int page,int size,Integer pageCount){
		List<LmCfg> lmCfgs = getAll();
		lmCfgs = SK_List.getPage(lmCfgs, page, size, pageCount);
		return lmCfgs;
	}
}