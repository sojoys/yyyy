package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户英雄
 */
public class YhyxCfg {

	public static final String TABLENAME = "用户英雄";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyxCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhyxCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> yxztidCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户_id */
	private int yh_id;
	
	/** 英雄id */
	private int yxid;
	
	/** 英雄lvl */
	private int yxlvl;
	
	/** 英雄exp */
	private int yxexp;
	
	/** 英雄状态id */
	private int yxztid;
	
	/** 兵数 */
	private int bs;
	
	
	public YhyxCfg() {
		super();
	}
	
	public YhyxCfg(int id, int yh_id, int yxid, int yxlvl, int yxexp, int yxztid, int bs) {
		super();
		this.id = id;
		this.yh_id = yh_id;
		this.yxid = yxid;
		this.yxlvl = yxlvl;
		this.yxexp = yxexp;
		this.yxztid = yxztid;
		this.bs = bs;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	public int getYxid() {
		return yxid;
	}
	
	public void setYxid(int yxid) {
		this.yxid = yxid;
	}
	public int getYxlvl() {
		return yxlvl;
	}
	
	public void setYxlvl(int yxlvl) {
		this.yxlvl = yxlvl;
	}
	public int getYxexp() {
		return yxexp;
	}
	
	public void setYxexp(int yxexp) {
		this.yxexp = yxexp;
	}
	public int getYxztid() {
		return yxztid;
	}
	
	public void setYxztid(int yxztid) {
		this.yxztid = yxztid;
	}
	public int getBs() {
		return bs;
	}
	
	public void setBs(int bs) {
		this.bs = bs;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhyxCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhyxCfg> yhyxCfgs = new ArrayList<YhyxCfg>();
		for (Map<String, Object> map : list) {
			yhyxCfgs.add(createForColumnNameMap(map));
		}
		return yhyxCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhyxCfg createForColumnNameMap(Map<String, Object> map){
    	YhyxCfg obj = new YhyxCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
	    obj.yxid = SK_Map.getInt("英雄id", map);
	    obj.yxlvl = SK_Map.getInt("英雄lvl", map);
	    obj.yxexp = SK_Map.getInt("英雄exp", map);
	    obj.yxztid = SK_Map.getInt("英雄状态id", map);
	    obj.bs = SK_Map.getInt("兵数", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yh_id);
	    SK_OutputStream.writeInt(out,this.yxid);
	    SK_OutputStream.writeInt(out,this.yxlvl);
	    SK_OutputStream.writeInt(out,this.yxexp);
	    SK_OutputStream.writeInt(out,this.yxztid);
	    SK_OutputStream.writeInt(out,this.bs);
    }
    
     public static YhyxCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhyxCfg yhyxCfg = new YhyxCfg();
	    yhyxCfg.id = SK_InputStream.readInt(in,null);
	    yhyxCfg.yh_id = SK_InputStream.readInt(in,null);
	    yhyxCfg.yxid = SK_InputStream.readInt(in,null);
	    yhyxCfg.yxlvl = SK_InputStream.readInt(in,null);
	    yhyxCfg.yxexp = SK_InputStream.readInt(in,null);
	    yhyxCfg.yxztid = SK_InputStream.readInt(in,null);
	    yhyxCfg.bs = SK_InputStream.readInt(in,null);
	    return yhyxCfg;
     }
    
    public static List<YhyxCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhyxCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhyxCfg.TABLENAME);
	}
	
	public static List<YhyxCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhyxCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhyxCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName;
		List<YhyxCfg> yhyxCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyxCfgs = YhyxCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxCfgs;
	}
	
	public static List<YhyxCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhyxCfg> yhyxCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhyxCfgs.size());
			for(YhyxCfg yhyxCfg : yhyxCfgs){
				yhyxCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhyxCfg yhyxCfg){
		idCache.put(SK_Plus.b(yhyxCfg.getId()).e(),yhyxCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhyxCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhyxCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhyxCfg.getYh_id()).e(),yh_idset);
		Set<String> yxztidset = yxztidCache.get(String.valueOf(SK_Plus.b(yhyxCfg.getYxztid()).e()));
		if(yxztidset == null){
			yxztidset = new HashSet<String>();
		}
		yxztidset.add(String.valueOf(yhyxCfg.getId()));
		yxztidCache.put(SK_Plus.b(yhyxCfg.getYxztid()).e(),yxztidset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhyxCfg getById(int id){
		YhyxCfg yhyxCfg = null;
		String key = SK_Plus.b(id).e();
		yhyxCfg = idCache.get(key);
		
		return yhyxCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhyxCfg> getByYh_id(int yh_id){
		List<YhyxCfg> yhyxCfgs = new ArrayList<YhyxCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhyxCfg yhyxCfg = null;
			for (String k : keys) {
				yhyxCfg = getById(Integer.valueOf(k));
				if (yhyxCfg == null) continue;
					yhyxCfgs.add(yhyxCfg);
			}
		}
		return yhyxCfgs;
	}
	
	public static List<YhyxCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhyxCfg> yhyxCfgs = getByYh_id(yh_id);
		yhyxCfgs = SK_List.getPage(yhyxCfgs, page, size, pageCount);
		return yhyxCfgs;
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<YhyxCfg> getByYxztid(int yxztid){
		List<YhyxCfg> yhyxCfgs = new ArrayList<YhyxCfg>();
		String key = SK_Plus.b(yxztid).e();
		Set<String> keys = yxztidCache.get(key);
		if(keys != null){
			YhyxCfg yhyxCfg = null;
			for (String k : keys) {
				yhyxCfg = getById(Integer.valueOf(k));
				if (yhyxCfg == null) continue;
					yhyxCfgs.add(yhyxCfg);
			}
		}
		return yhyxCfgs;
	}
	
	public static List<YhyxCfg> getByPageYxztid(int yxztid,int page,int size,Integer pageCount){
		List<YhyxCfg> yhyxCfgs = getByYxztid(yxztid);
		yhyxCfgs = SK_List.getPage(yhyxCfgs, page, size, pageCount);
		return yhyxCfgs;
	}
	
	public static List<YhyxCfg> getAll(){
		return new ArrayList<YhyxCfg>(idCache.values());
	}
	
	public static List<YhyxCfg> getAll(int page,int size,Integer pageCount){
		List<YhyxCfg> yhyxCfgs = getAll();
		yhyxCfgs = SK_List.getPage(yhyxCfgs, page, size, pageCount);
		return yhyxCfgs;
	}
}