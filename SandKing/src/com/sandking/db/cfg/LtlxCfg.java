package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 聊天类型
 */
public class LtlxCfg {

	public static final String TABLENAME = "聊天类型";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtlxCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, LtlxCfg> idCache = SK_Collections.newSortedMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public LtlxCfg() {
		super();
	}
	
	public LtlxCfg(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<LtlxCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<LtlxCfg> ltlxCfgs = new ArrayList<LtlxCfg>();
		for (Map<String, Object> map : list) {
			ltlxCfgs.add(createForColumnNameMap(map));
		}
		return ltlxCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static LtlxCfg createForColumnNameMap(Map<String, Object> map){
    	LtlxCfg obj = new LtlxCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
    }
    
     public static LtlxCfg forStream(ByteArrayInputStream in) throws Exception {
     	LtlxCfg ltlxCfg = new LtlxCfg();
	    ltlxCfg.id = SK_InputStream.readInt(in,null);
	    ltlxCfg.mc = SK_InputStream.readString(in,null);
	    return ltlxCfg;
     }
    
    public static List<LtlxCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<LtlxCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,LtlxCfg.TABLENAME);
	}
	
	public static List<LtlxCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<LtlxCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<LtlxCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<LtlxCfg> ltlxCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			ltlxCfgs = LtlxCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return ltlxCfgs;
	}
	
	public static List<LtlxCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<LtlxCfg> ltlxCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, ltlxCfgs.size());
			for(LtlxCfg ltlxCfg : ltlxCfgs){
				ltlxCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(LtlxCfg ltlxCfg){
		idCache.put(SK_Plus.b(ltlxCfg.getId()).e(),ltlxCfg);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static LtlxCfg getById(int id){
		LtlxCfg ltlxCfg = null;
		String key = SK_Plus.b(id).e();
		ltlxCfg = idCache.get(key);
		
		return ltlxCfg;
	}
	
	
	public static List<LtlxCfg> getAll(){
		return new ArrayList<LtlxCfg>(idCache.values());
	}
	
	public static List<LtlxCfg> getAll(int page,int size,Integer pageCount){
		List<LtlxCfg> ltlxCfgs = getAll();
		ltlxCfgs = SK_List.getPage(ltlxCfgs, page, size, pageCount);
		return ltlxCfgs;
	}
}