package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户陷阱
 */
public class YhxjCfg {

	public static final String TABLENAME = "用户陷阱";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhxjCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhxjCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 陷阱id */
	private int xjid;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public YhxjCfg() {
		super();
	}
	
	public YhxjCfg(int id, int xjid, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.xjid = xjid;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getXjid() {
		return xjid;
	}
	
	public void setXjid(int xjid) {
		this.xjid = xjid;
	}
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhxjCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhxjCfg> yhxjCfgs = new ArrayList<YhxjCfg>();
		for (Map<String, Object> map : list) {
			yhxjCfgs.add(createForColumnNameMap(map));
		}
		return yhxjCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhxjCfg createForColumnNameMap(Map<String, Object> map){
    	YhxjCfg obj = new YhxjCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.xjid = SK_Map.getInt("陷阱id", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.xjid);
	    SK_OutputStream.writeInt(out,this.x);
	    SK_OutputStream.writeInt(out,this.y);
	    SK_OutputStream.writeInt(out,this.yh_id);
    }
    
     public static YhxjCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhxjCfg yhxjCfg = new YhxjCfg();
	    yhxjCfg.id = SK_InputStream.readInt(in,null);
	    yhxjCfg.xjid = SK_InputStream.readInt(in,null);
	    yhxjCfg.x = SK_InputStream.readInt(in,null);
	    yhxjCfg.y = SK_InputStream.readInt(in,null);
	    yhxjCfg.yh_id = SK_InputStream.readInt(in,null);
	    return yhxjCfg;
     }
    
    public static List<YhxjCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhxjCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhxjCfg.TABLENAME);
	}
	
	public static List<YhxjCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhxjCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhxjCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,陷阱id,x,y,用户_id FROM " + tableName;
		List<YhxjCfg> yhxjCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhxjCfgs = YhxjCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhxjCfgs;
	}
	
	public static List<YhxjCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhxjCfg> yhxjCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhxjCfgs.size());
			for(YhxjCfg yhxjCfg : yhxjCfgs){
				yhxjCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhxjCfg yhxjCfg){
		idCache.put(SK_Plus.b(yhxjCfg.getId()).e(),yhxjCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhxjCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhxjCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhxjCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhxjCfg getById(int id){
		YhxjCfg yhxjCfg = null;
		String key = SK_Plus.b(id).e();
		yhxjCfg = idCache.get(key);
		
		return yhxjCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhxjCfg> getByYh_id(int yh_id){
		List<YhxjCfg> yhxjCfgs = new ArrayList<YhxjCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhxjCfg yhxjCfg = null;
			for (String k : keys) {
				yhxjCfg = getById(Integer.valueOf(k));
				if (yhxjCfg == null) continue;
					yhxjCfgs.add(yhxjCfg);
			}
		}
		return yhxjCfgs;
	}
	
	public static List<YhxjCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhxjCfg> yhxjCfgs = getByYh_id(yh_id);
		yhxjCfgs = SK_List.getPage(yhxjCfgs, page, size, pageCount);
		return yhxjCfgs;
	}
	
	public static List<YhxjCfg> getAll(){
		return new ArrayList<YhxjCfg>(idCache.values());
	}
	
	public static List<YhxjCfg> getAll(int page,int size,Integer pageCount){
		List<YhxjCfg> yhxjCfgs = getAll();
		yhxjCfgs = SK_List.getPage(yhxjCfgs, page, size, pageCount);
		return yhxjCfgs;
	}
}