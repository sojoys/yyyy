package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户映射
 */
public class YhysCfg {

	public static final String TABLENAME = "用户映射";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhysCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhysCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 建筑 */
	private String jz;
	
	/** 障碍 */
	private String za;
	
	/** 装饰 */
	private String zs;
	
	/** 陷阱 */
	private String xj;
	
	/** 用户_id */
	private int yh_id;
	
	
	public YhysCfg() {
		super();
	}
	
	public YhysCfg(int id, String jz, String za, String zs, String xj, int yh_id) {
		super();
		this.id = id;
		this.jz = jz;
		this.za = za;
		this.zs = zs;
		this.xj = xj;
		this.yh_id = yh_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getJz() {
		return jz;
	}
	
	public void setJz(String jz) {
		this.jz = jz;
	}
	public String getZa() {
		return za;
	}
	
	public void setZa(String za) {
		this.za = za;
	}
	public String getZs() {
		return zs;
	}
	
	public void setZs(String zs) {
		this.zs = zs;
	}
	public String getXj() {
		return xj;
	}
	
	public void setXj(String xj) {
		this.xj = xj;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhysCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhysCfg> yhysCfgs = new ArrayList<YhysCfg>();
		for (Map<String, Object> map : list) {
			yhysCfgs.add(createForColumnNameMap(map));
		}
		return yhysCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhysCfg createForColumnNameMap(Map<String, Object> map){
    	YhysCfg obj = new YhysCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jz = SK_Map.getString("建筑", map);
	    obj.za = SK_Map.getString("障碍", map);
	    obj.zs = SK_Map.getString("装饰", map);
	    obj.xj = SK_Map.getString("陷阱", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.jz);
	    SK_OutputStream.writeString(out,this.za);
	    SK_OutputStream.writeString(out,this.zs);
	    SK_OutputStream.writeString(out,this.xj);
	    SK_OutputStream.writeInt(out,this.yh_id);
    }
    
     public static YhysCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhysCfg yhysCfg = new YhysCfg();
	    yhysCfg.id = SK_InputStream.readInt(in,null);
	    yhysCfg.jz = SK_InputStream.readString(in,null);
	    yhysCfg.za = SK_InputStream.readString(in,null);
	    yhysCfg.zs = SK_InputStream.readString(in,null);
	    yhysCfg.xj = SK_InputStream.readString(in,null);
	    yhysCfg.yh_id = SK_InputStream.readInt(in,null);
	    return yhysCfg;
     }
    
    public static List<YhysCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhysCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhysCfg.TABLENAME);
	}
	
	public static List<YhysCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhysCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhysCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑,障碍,装饰,陷阱,用户_id FROM " + tableName;
		List<YhysCfg> yhysCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhysCfgs = YhysCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhysCfgs;
	}
	
	public static List<YhysCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhysCfg> yhysCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhysCfgs.size());
			for(YhysCfg yhysCfg : yhysCfgs){
				yhysCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhysCfg yhysCfg){
		idCache.put(SK_Plus.b(yhysCfg.getId()).e(),yhysCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhysCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhysCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhysCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhysCfg getById(int id){
		YhysCfg yhysCfg = null;
		String key = SK_Plus.b(id).e();
		yhysCfg = idCache.get(key);
		
		return yhysCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhysCfg> getByYh_id(int yh_id){
		List<YhysCfg> yhysCfgs = new ArrayList<YhysCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhysCfg yhysCfg = null;
			for (String k : keys) {
				yhysCfg = getById(Integer.valueOf(k));
				if (yhysCfg == null) continue;
					yhysCfgs.add(yhysCfg);
			}
		}
		return yhysCfgs;
	}
	
	public static List<YhysCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhysCfg> yhysCfgs = getByYh_id(yh_id);
		yhysCfgs = SK_List.getPage(yhysCfgs, page, size, pageCount);
		return yhysCfgs;
	}
	
	public static List<YhysCfg> getAll(){
		return new ArrayList<YhysCfg>(idCache.values());
	}
	
	public static List<YhysCfg> getAll(int page,int size,Integer pageCount){
		List<YhysCfg> yhysCfgs = getAll();
		yhysCfgs = SK_List.getPage(yhysCfgs, page, size, pageCount);
		return yhysCfgs;
	}
}