package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 英雄状态
 */
public class YxztCfg {

	public static final String TABLENAME = "英雄状态";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxztCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YxztCfg> idCache = SK_Collections.newSortedMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public YxztCfg() {
		super();
	}
	
	public YxztCfg(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YxztCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YxztCfg> yxztCfgs = new ArrayList<YxztCfg>();
		for (Map<String, Object> map : list) {
			yxztCfgs.add(createForColumnNameMap(map));
		}
		return yxztCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YxztCfg createForColumnNameMap(Map<String, Object> map){
    	YxztCfg obj = new YxztCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
    }
    
     public static YxztCfg forStream(ByteArrayInputStream in) throws Exception {
     	YxztCfg yxztCfg = new YxztCfg();
	    yxztCfg.id = SK_InputStream.readInt(in,null);
	    yxztCfg.mc = SK_InputStream.readString(in,null);
	    return yxztCfg;
     }
    
    public static List<YxztCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YxztCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YxztCfg.TABLENAME);
	}
	
	public static List<YxztCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YxztCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YxztCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<YxztCfg> yxztCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxztCfgs = YxztCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxztCfgs;
	}
	
	public static List<YxztCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YxztCfg> yxztCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yxztCfgs.size());
			for(YxztCfg yxztCfg : yxztCfgs){
				yxztCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YxztCfg yxztCfg){
		idCache.put(SK_Plus.b(yxztCfg.getId()).e(),yxztCfg);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YxztCfg getById(int id){
		YxztCfg yxztCfg = null;
		String key = SK_Plus.b(id).e();
		yxztCfg = idCache.get(key);
		
		return yxztCfg;
	}
	
	
	public static List<YxztCfg> getAll(){
		return new ArrayList<YxztCfg>(idCache.values());
	}
	
	public static List<YxztCfg> getAll(int page,int size,Integer pageCount){
		List<YxztCfg> yxztCfgs = getAll();
		yxztCfgs = SK_List.getPage(yxztCfgs, page, size, pageCount);
		return yxztCfgs;
	}
}