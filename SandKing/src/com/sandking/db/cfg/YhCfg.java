package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.tools.SK_IndexMap;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户
 */
public class YhCfg {

	public static final String TABLENAME = "用户";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhCfg> idCache = SK_Collections.newSortedMap();
	static final SK_IndexMap<String, String> ncCache = new SK_IndexMap<String, String>();
	static final SK_IndexMap<String, String> zhCache = new SK_IndexMap<String, String>();
	static final SK_IndexMap<String, String> zhmmCache = new SK_IndexMap<String, String>();
	static final Map<String, Set<String>> yhlx_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> fwq_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> qdCache = SK_Collections.newMap();
	static final Map<String, Set<String>> lm_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 昵称 */
	private String nc;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	/** 最后登录时间 */
	private java.util.Date zhdlsj;
	
	/** 语言 */
	private String yy;
	
	/** 账号 */
	private String zh;
	
	/** 密码 */
	private String mm;
	
	/** 宝石 */
	private int bs;
	
	/** 假宝石 */
	private int jbs;
	
	/** 金币 */
	private int jb;
	
	/** 水 */
	private int s;
	
	/** 修炼点 */
	private int xld;
	
	/** 设备 */
	private String sb;
	
	/** 设备系统 */
	private String sbxt;
	
	/** 分辨率 */
	private String fbl;
	
	/** 渠道 */
	private String qd;
	
	/** 用户类型_id */
	private int yhlx_id;
	
	/** 用户lvl */
	private int yhlvl;
	
	/** 用户lvl_exp */
	private int yhlvl_exp;
	
	/** VIP */
	private int vIP;
	
	/** VIP_exp */
	private int vIP_exp;
	
	/** 充值金额 */
	private int czje;
	
	/** 幸运 */
	private int xy;
	
	/** 客户端版本 */
	private String khdbb;
	
	/** 服务器_id */
	private int fwq_id;
	
	/** 主城lvl */
	private int zclvl;
	
	/** 锁定 */
	private boolean sd;
	
	/** 联盟_id */
	private int lm_id;
	
	
	public YhCfg() {
		super();
	}
	
	public YhCfg(int id, String nc, java.util.Date cjsj, java.util.Date zhdlsj, String yy, String zh, String mm, int bs, int jbs, int jb, int s, int xld, String sb, String sbxt, String fbl, String qd, int yhlx_id, int yhlvl, int yhlvl_exp, int vIP, int vIP_exp, int czje, int xy, String khdbb, int fwq_id, int zclvl, boolean sd, int lm_id) {
		super();
		this.id = id;
		this.nc = nc;
		this.cjsj = cjsj;
		this.zhdlsj = zhdlsj;
		this.yy = yy;
		this.zh = zh;
		this.mm = mm;
		this.bs = bs;
		this.jbs = jbs;
		this.jb = jb;
		this.s = s;
		this.xld = xld;
		this.sb = sb;
		this.sbxt = sbxt;
		this.fbl = fbl;
		this.qd = qd;
		this.yhlx_id = yhlx_id;
		this.yhlvl = yhlvl;
		this.yhlvl_exp = yhlvl_exp;
		this.vIP = vIP;
		this.vIP_exp = vIP_exp;
		this.czje = czje;
		this.xy = xy;
		this.khdbb = khdbb;
		this.fwq_id = fwq_id;
		this.zclvl = zclvl;
		this.sd = sd;
		this.lm_id = lm_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getNc() {
		return nc;
	}
	
	public void setNc(String nc) {
		this.nc = nc;
	}
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}
	public java.util.Date getZhdlsj() {
		return zhdlsj;
	}
	
	public void setZhdlsj(java.util.Date zhdlsj) {
		this.zhdlsj = zhdlsj;
	}
	public String getYy() {
		return yy;
	}
	
	public void setYy(String yy) {
		this.yy = yy;
	}
	public String getZh() {
		return zh;
	}
	
	public void setZh(String zh) {
		this.zh = zh;
	}
	public String getMm() {
		return mm;
	}
	
	public void setMm(String mm) {
		this.mm = mm;
	}
	public int getBs() {
		return bs;
	}
	
	public void setBs(int bs) {
		this.bs = bs;
	}
	public int getJbs() {
		return jbs;
	}
	
	public void setJbs(int jbs) {
		this.jbs = jbs;
	}
	public int getJb() {
		return jb;
	}
	
	public void setJb(int jb) {
		this.jb = jb;
	}
	public int getS() {
		return s;
	}
	
	public void setS(int s) {
		this.s = s;
	}
	public int getXld() {
		return xld;
	}
	
	public void setXld(int xld) {
		this.xld = xld;
	}
	public String getSb() {
		return sb;
	}
	
	public void setSb(String sb) {
		this.sb = sb;
	}
	public String getSbxt() {
		return sbxt;
	}
	
	public void setSbxt(String sbxt) {
		this.sbxt = sbxt;
	}
	public String getFbl() {
		return fbl;
	}
	
	public void setFbl(String fbl) {
		this.fbl = fbl;
	}
	public String getQd() {
		return qd;
	}
	
	public void setQd(String qd) {
		this.qd = qd;
	}
	public int getYhlx_id() {
		return yhlx_id;
	}
	
	public void setYhlx_id(int yhlx_id) {
		this.yhlx_id = yhlx_id;
	}
	public int getYhlvl() {
		return yhlvl;
	}
	
	public void setYhlvl(int yhlvl) {
		this.yhlvl = yhlvl;
	}
	public int getYhlvl_exp() {
		return yhlvl_exp;
	}
	
	public void setYhlvl_exp(int yhlvl_exp) {
		this.yhlvl_exp = yhlvl_exp;
	}
	public int getVIP() {
		return vIP;
	}
	
	public void setVIP(int vIP) {
		this.vIP = vIP;
	}
	public int getVIP_exp() {
		return vIP_exp;
	}
	
	public void setVIP_exp(int vIP_exp) {
		this.vIP_exp = vIP_exp;
	}
	public int getCzje() {
		return czje;
	}
	
	public void setCzje(int czje) {
		this.czje = czje;
	}
	public int getXy() {
		return xy;
	}
	
	public void setXy(int xy) {
		this.xy = xy;
	}
	public String getKhdbb() {
		return khdbb;
	}
	
	public void setKhdbb(String khdbb) {
		this.khdbb = khdbb;
	}
	public int getFwq_id() {
		return fwq_id;
	}
	
	public void setFwq_id(int fwq_id) {
		this.fwq_id = fwq_id;
	}
	public int getZclvl() {
		return zclvl;
	}
	
	public void setZclvl(int zclvl) {
		this.zclvl = zclvl;
	}
	public boolean getSd() {
		return sd;
	}
	
	public void setSd(boolean sd) {
		this.sd = sd;
	}
	public int getLm_id() {
		return lm_id;
	}
	
	public void setLm_id(int lm_id) {
		this.lm_id = lm_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhCfg> yhCfgs = new ArrayList<YhCfg>();
		for (Map<String, Object> map : list) {
			yhCfgs.add(createForColumnNameMap(map));
		}
		return yhCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhCfg createForColumnNameMap(Map<String, Object> map){
    	YhCfg obj = new YhCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.nc = SK_Map.getString("昵称", map);
	    obj.cjsj = SK_Map.getDate("创建时间", map);
	    obj.zhdlsj = SK_Map.getDate("最后登录时间", map);
	    obj.yy = SK_Map.getString("语言", map);
	    obj.zh = SK_Map.getString("账号", map);
	    obj.mm = SK_Map.getString("密码", map);
	    obj.bs = SK_Map.getInt("宝石", map);
	    obj.jbs = SK_Map.getInt("假宝石", map);
	    obj.jb = SK_Map.getInt("金币", map);
	    obj.s = SK_Map.getInt("水", map);
	    obj.xld = SK_Map.getInt("修炼点", map);
	    obj.sb = SK_Map.getString("设备", map);
	    obj.sbxt = SK_Map.getString("设备系统", map);
	    obj.fbl = SK_Map.getString("分辨率", map);
	    obj.qd = SK_Map.getString("渠道", map);
	    obj.yhlx_id = SK_Map.getInt("用户类型_id", map);
	    obj.yhlvl = SK_Map.getInt("用户lvl", map);
	    obj.yhlvl_exp = SK_Map.getInt("用户lvl_exp", map);
	    obj.vIP = SK_Map.getInt("VIP", map);
	    obj.vIP_exp = SK_Map.getInt("VIP_exp", map);
	    obj.czje = SK_Map.getInt("充值金额", map);
	    obj.xy = SK_Map.getInt("幸运", map);
	    obj.khdbb = SK_Map.getString("客户端版本", map);
	    obj.fwq_id = SK_Map.getInt("服务器_id", map);
	    obj.zclvl = SK_Map.getInt("主城lvl", map);
	    obj.lm_id = SK_Map.getInt("联盟_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.nc);
	    SK_OutputStream.writeDate(out,this.cjsj);
	    SK_OutputStream.writeDate(out,this.zhdlsj);
	    SK_OutputStream.writeString(out,this.yy);
	    SK_OutputStream.writeString(out,this.zh);
	    SK_OutputStream.writeString(out,this.mm);
	    SK_OutputStream.writeInt(out,this.bs);
	    SK_OutputStream.writeInt(out,this.jbs);
	    SK_OutputStream.writeInt(out,this.jb);
	    SK_OutputStream.writeInt(out,this.s);
	    SK_OutputStream.writeInt(out,this.xld);
	    SK_OutputStream.writeString(out,this.sb);
	    SK_OutputStream.writeString(out,this.sbxt);
	    SK_OutputStream.writeString(out,this.fbl);
	    SK_OutputStream.writeString(out,this.qd);
	    SK_OutputStream.writeInt(out,this.yhlx_id);
	    SK_OutputStream.writeInt(out,this.yhlvl);
	    SK_OutputStream.writeInt(out,this.yhlvl_exp);
	    SK_OutputStream.writeInt(out,this.vIP);
	    SK_OutputStream.writeInt(out,this.vIP_exp);
	    SK_OutputStream.writeInt(out,this.czje);
	    SK_OutputStream.writeInt(out,this.xy);
	    SK_OutputStream.writeString(out,this.khdbb);
	    SK_OutputStream.writeInt(out,this.fwq_id);
	    SK_OutputStream.writeInt(out,this.zclvl);
	    SK_OutputStream.writeInt(out,this.lm_id);
    }
    
     public static YhCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhCfg yhCfg = new YhCfg();
	    yhCfg.id = SK_InputStream.readInt(in,null);
	    yhCfg.nc = SK_InputStream.readString(in,null);
	    yhCfg.cjsj = SK_InputStream.readDate(in,null);
	    yhCfg.zhdlsj = SK_InputStream.readDate(in,null);
	    yhCfg.yy = SK_InputStream.readString(in,null);
	    yhCfg.zh = SK_InputStream.readString(in,null);
	    yhCfg.mm = SK_InputStream.readString(in,null);
	    yhCfg.bs = SK_InputStream.readInt(in,null);
	    yhCfg.jbs = SK_InputStream.readInt(in,null);
	    yhCfg.jb = SK_InputStream.readInt(in,null);
	    yhCfg.s = SK_InputStream.readInt(in,null);
	    yhCfg.xld = SK_InputStream.readInt(in,null);
	    yhCfg.sb = SK_InputStream.readString(in,null);
	    yhCfg.sbxt = SK_InputStream.readString(in,null);
	    yhCfg.fbl = SK_InputStream.readString(in,null);
	    yhCfg.qd = SK_InputStream.readString(in,null);
	    yhCfg.yhlx_id = SK_InputStream.readInt(in,null);
	    yhCfg.yhlvl = SK_InputStream.readInt(in,null);
	    yhCfg.yhlvl_exp = SK_InputStream.readInt(in,null);
	    yhCfg.vIP = SK_InputStream.readInt(in,null);
	    yhCfg.vIP_exp = SK_InputStream.readInt(in,null);
	    yhCfg.czje = SK_InputStream.readInt(in,null);
	    yhCfg.xy = SK_InputStream.readInt(in,null);
	    yhCfg.khdbb = SK_InputStream.readString(in,null);
	    yhCfg.fwq_id = SK_InputStream.readInt(in,null);
	    yhCfg.zclvl = SK_InputStream.readInt(in,null);
	    yhCfg.lm_id = SK_InputStream.readInt(in,null);
	    return yhCfg;
     }
    
    public static List<YhCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhCfg.TABLENAME);
	}
	
	public static List<YhCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName;
		List<YhCfg> yhCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhCfgs = YhCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhCfgs;
	}
	
	public static List<YhCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhCfg> yhCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhCfgs.size());
			for(YhCfg yhCfg : yhCfgs){
				yhCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhCfg yhCfg){
		idCache.put(SK_Plus.b(yhCfg.getId()).e(),yhCfg);
		ncCache.put(SK_Plus.b(yhCfg.getNc()).e(),String.valueOf(yhCfg.getId()));
		zhCache.put(SK_Plus.b(yhCfg.getZh()).e(),String.valueOf(yhCfg.getId()));
		zhmmCache.put(SK_Plus.b(yhCfg.getZh(),yhCfg.getMm()).e(),String.valueOf(yhCfg.getId()));
		Set<String> yhlx_idset = yhlx_idCache.get(String.valueOf(SK_Plus.b(yhCfg.getYhlx_id()).e()));
		if(yhlx_idset == null){
			yhlx_idset = new HashSet<String>();
		}
		yhlx_idset.add(String.valueOf(yhCfg.getId()));
		yhlx_idCache.put(SK_Plus.b(yhCfg.getYhlx_id()).e(),yhlx_idset);
		Set<String> fwq_idset = fwq_idCache.get(String.valueOf(SK_Plus.b(yhCfg.getFwq_id()).e()));
		if(fwq_idset == null){
			fwq_idset = new HashSet<String>();
		}
		fwq_idset.add(String.valueOf(yhCfg.getId()));
		fwq_idCache.put(SK_Plus.b(yhCfg.getFwq_id()).e(),fwq_idset);
		Set<String> qdset = qdCache.get(String.valueOf(SK_Plus.b(yhCfg.getQd()).e()));
		if(qdset == null){
			qdset = new HashSet<String>();
		}
		qdset.add(String.valueOf(yhCfg.getId()));
		qdCache.put(SK_Plus.b(yhCfg.getQd()).e(),qdset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(yhCfg.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(yhCfg.getId()));
		lm_idCache.put(SK_Plus.b(yhCfg.getLm_id()).e(),lm_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhCfg getById(int id){
		YhCfg yhCfg = null;
		String key = SK_Plus.b(id).e();
		yhCfg = idCache.get(key);
		
		return yhCfg;
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static YhCfg getByNc(String nc){
		YhCfg yhCfg = null;
		String key = SK_Plus.b(nc).e();
		key = ncCache.getValseByKey(key);
		if(key!=null){
			yhCfg = idCache.get(key);	
		} 
		return yhCfg;
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static YhCfg getByZh(String zh){
		YhCfg yhCfg = null;
		String key = SK_Plus.b(zh).e();
		key = zhCache.getValseByKey(key);
		if(key!=null){
			yhCfg = idCache.get(key);	
		} 
		return yhCfg;
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static YhCfg getByZhMm(String zh, String mm){
		YhCfg yhCfg = null;
		String key = SK_Plus.b(zh,mm).e();
		key = zhmmCache.getValseByKey(key);
		if(key!=null){
			yhCfg = idCache.get(key);	
		} 
		return yhCfg;
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<YhCfg> getByYhlx_id(int yhlx_id){
		List<YhCfg> yhCfgs = new ArrayList<YhCfg>();
		String key = SK_Plus.b(yhlx_id).e();
		Set<String> keys = yhlx_idCache.get(key);
		if(keys != null){
			YhCfg yhCfg = null;
			for (String k : keys) {
				yhCfg = getById(Integer.valueOf(k));
				if (yhCfg == null) continue;
					yhCfgs.add(yhCfg);
			}
		}
		return yhCfgs;
	}
	
	public static List<YhCfg> getByPageYhlx_id(int yhlx_id,int page,int size,Integer pageCount){
		List<YhCfg> yhCfgs = getByYhlx_id(yhlx_id);
		yhCfgs = SK_List.getPage(yhCfgs, page, size, pageCount);
		return yhCfgs;
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<YhCfg> getByFwq_id(int fwq_id){
		List<YhCfg> yhCfgs = new ArrayList<YhCfg>();
		String key = SK_Plus.b(fwq_id).e();
		Set<String> keys = fwq_idCache.get(key);
		if(keys != null){
			YhCfg yhCfg = null;
			for (String k : keys) {
				yhCfg = getById(Integer.valueOf(k));
				if (yhCfg == null) continue;
					yhCfgs.add(yhCfg);
			}
		}
		return yhCfgs;
	}
	
	public static List<YhCfg> getByPageFwq_id(int fwq_id,int page,int size,Integer pageCount){
		List<YhCfg> yhCfgs = getByFwq_id(fwq_id);
		yhCfgs = SK_List.getPage(yhCfgs, page, size, pageCount);
		return yhCfgs;
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<YhCfg> getByQd(String qd){
		List<YhCfg> yhCfgs = new ArrayList<YhCfg>();
		String key = SK_Plus.b(qd).e();
		Set<String> keys = qdCache.get(key);
		if(keys != null){
			YhCfg yhCfg = null;
			for (String k : keys) {
				yhCfg = getById(Integer.valueOf(k));
				if (yhCfg == null) continue;
					yhCfgs.add(yhCfg);
			}
		}
		return yhCfgs;
	}
	
	public static List<YhCfg> getByPageQd(String qd,int page,int size,Integer pageCount){
		List<YhCfg> yhCfgs = getByQd(qd);
		yhCfgs = SK_List.getPage(yhCfgs, page, size, pageCount);
		return yhCfgs;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<YhCfg> getByLm_id(int lm_id){
		List<YhCfg> yhCfgs = new ArrayList<YhCfg>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			YhCfg yhCfg = null;
			for (String k : keys) {
				yhCfg = getById(Integer.valueOf(k));
				if (yhCfg == null) continue;
					yhCfgs.add(yhCfg);
			}
		}
		return yhCfgs;
	}
	
	public static List<YhCfg> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<YhCfg> yhCfgs = getByLm_id(lm_id);
		yhCfgs = SK_List.getPage(yhCfgs, page, size, pageCount);
		return yhCfgs;
	}
	
	public static List<YhCfg> getAll(){
		return new ArrayList<YhCfg>(idCache.values());
	}
	
	public static List<YhCfg> getAll(int page,int size,Integer pageCount){
		List<YhCfg> yhCfgs = getAll();
		yhCfgs = SK_List.getPage(yhCfgs, page, size, pageCount);
		return yhCfgs;
	}
}