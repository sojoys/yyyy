package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户建筑
 */
public class YhjzCfg {

	public static final String TABLENAME = "用户建筑";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhjzCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhjzCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 建筑id */
	private int jzid;
	
	/** 建筑lvl */
	private int jzlvl;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public YhjzCfg() {
		super();
	}
	
	public YhjzCfg(int id, int jzid, int jzlvl, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.jzid = jzid;
		this.jzlvl = jzlvl;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getJzid() {
		return jzid;
	}
	
	public void setJzid(int jzid) {
		this.jzid = jzid;
	}
	public int getJzlvl() {
		return jzlvl;
	}
	
	public void setJzlvl(int jzlvl) {
		this.jzlvl = jzlvl;
	}
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhjzCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhjzCfg> yhjzCfgs = new ArrayList<YhjzCfg>();
		for (Map<String, Object> map : list) {
			yhjzCfgs.add(createForColumnNameMap(map));
		}
		return yhjzCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhjzCfg createForColumnNameMap(Map<String, Object> map){
    	YhjzCfg obj = new YhjzCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jzid = SK_Map.getInt("建筑id", map);
	    obj.jzlvl = SK_Map.getInt("建筑lvl", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.jzid);
	    SK_OutputStream.writeInt(out,this.jzlvl);
	    SK_OutputStream.writeInt(out,this.x);
	    SK_OutputStream.writeInt(out,this.y);
	    SK_OutputStream.writeInt(out,this.yh_id);
    }
    
     public static YhjzCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhjzCfg yhjzCfg = new YhjzCfg();
	    yhjzCfg.id = SK_InputStream.readInt(in,null);
	    yhjzCfg.jzid = SK_InputStream.readInt(in,null);
	    yhjzCfg.jzlvl = SK_InputStream.readInt(in,null);
	    yhjzCfg.x = SK_InputStream.readInt(in,null);
	    yhjzCfg.y = SK_InputStream.readInt(in,null);
	    yhjzCfg.yh_id = SK_InputStream.readInt(in,null);
	    return yhjzCfg;
     }
    
    public static List<YhjzCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhjzCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhjzCfg.TABLENAME);
	}
	
	public static List<YhjzCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhjzCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhjzCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName;
		List<YhjzCfg> yhjzCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhjzCfgs = YhjzCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjzCfgs;
	}
	
	public static List<YhjzCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhjzCfg> yhjzCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhjzCfgs.size());
			for(YhjzCfg yhjzCfg : yhjzCfgs){
				yhjzCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhjzCfg yhjzCfg){
		idCache.put(SK_Plus.b(yhjzCfg.getId()).e(),yhjzCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhjzCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhjzCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhjzCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhjzCfg getById(int id){
		YhjzCfg yhjzCfg = null;
		String key = SK_Plus.b(id).e();
		yhjzCfg = idCache.get(key);
		
		return yhjzCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhjzCfg> getByYh_id(int yh_id){
		List<YhjzCfg> yhjzCfgs = new ArrayList<YhjzCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhjzCfg yhjzCfg = null;
			for (String k : keys) {
				yhjzCfg = getById(Integer.valueOf(k));
				if (yhjzCfg == null) continue;
					yhjzCfgs.add(yhjzCfg);
			}
		}
		return yhjzCfgs;
	}
	
	public static List<YhjzCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhjzCfg> yhjzCfgs = getByYh_id(yh_id);
		yhjzCfgs = SK_List.getPage(yhjzCfgs, page, size, pageCount);
		return yhjzCfgs;
	}
	
	public static List<YhjzCfg> getAll(){
		return new ArrayList<YhjzCfg>(idCache.values());
	}
	
	public static List<YhjzCfg> getAll(int page,int size,Integer pageCount){
		List<YhjzCfg> yhjzCfgs = getAll();
		yhjzCfgs = SK_List.getPage(yhjzCfgs, page, size, pageCount);
		return yhjzCfgs;
	}
}