package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 障碍队列
 */
public class ZadlCfg {

	public static final String TABLENAME = "障碍队列";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		ZadlCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, ZadlCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhza_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户障碍_id */
	private int yhza_id;
	
	
	public ZadlCfg() {
		super();
	}
	
	public ZadlCfg(int id, int yhza_id) {
		super();
		this.id = id;
		this.yhza_id = yhza_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYhza_id() {
		return yhza_id;
	}
	
	public void setYhza_id(int yhza_id) {
		this.yhza_id = yhza_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<ZadlCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<ZadlCfg> zadlCfgs = new ArrayList<ZadlCfg>();
		for (Map<String, Object> map : list) {
			zadlCfgs.add(createForColumnNameMap(map));
		}
		return zadlCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static ZadlCfg createForColumnNameMap(Map<String, Object> map){
    	ZadlCfg obj = new ZadlCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhza_id = SK_Map.getInt("用户障碍_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yhza_id);
    }
    
     public static ZadlCfg forStream(ByteArrayInputStream in) throws Exception {
     	ZadlCfg zadlCfg = new ZadlCfg();
	    zadlCfg.id = SK_InputStream.readInt(in,null);
	    zadlCfg.yhza_id = SK_InputStream.readInt(in,null);
	    return zadlCfg;
     }
    
    public static List<ZadlCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<ZadlCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,ZadlCfg.TABLENAME);
	}
	
	public static List<ZadlCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<ZadlCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<ZadlCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户障碍_id FROM " + tableName;
		List<ZadlCfg> zadlCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			zadlCfgs = ZadlCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadlCfgs;
	}
	
	public static List<ZadlCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<ZadlCfg> zadlCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, zadlCfgs.size());
			for(ZadlCfg zadlCfg : zadlCfgs){
				zadlCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(ZadlCfg zadlCfg){
		idCache.put(SK_Plus.b(zadlCfg.getId()).e(),zadlCfg);
		Set<String> yhza_idset = yhza_idCache.get(String.valueOf(SK_Plus.b(zadlCfg.getYhza_id()).e()));
		if(yhza_idset == null){
			yhza_idset = new HashSet<String>();
		}
		yhza_idset.add(String.valueOf(zadlCfg.getId()));
		yhza_idCache.put(SK_Plus.b(zadlCfg.getYhza_id()).e(),yhza_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static ZadlCfg getById(int id){
		ZadlCfg zadlCfg = null;
		String key = SK_Plus.b(id).e();
		zadlCfg = idCache.get(key);
		
		return zadlCfg;
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<ZadlCfg> getByYhza_id(int yhza_id){
		List<ZadlCfg> zadlCfgs = new ArrayList<ZadlCfg>();
		String key = SK_Plus.b(yhza_id).e();
		Set<String> keys = yhza_idCache.get(key);
		if(keys != null){
			ZadlCfg zadlCfg = null;
			for (String k : keys) {
				zadlCfg = getById(Integer.valueOf(k));
				if (zadlCfg == null) continue;
					zadlCfgs.add(zadlCfg);
			}
		}
		return zadlCfgs;
	}
	
	public static List<ZadlCfg> getByPageYhza_id(int yhza_id,int page,int size,Integer pageCount){
		List<ZadlCfg> zadlCfgs = getByYhza_id(yhza_id);
		zadlCfgs = SK_List.getPage(zadlCfgs, page, size, pageCount);
		return zadlCfgs;
	}
	
	public static List<ZadlCfg> getAll(){
		return new ArrayList<ZadlCfg>(idCache.values());
	}
	
	public static List<ZadlCfg> getAll(int page,int size,Integer pageCount){
		List<ZadlCfg> zadlCfgs = getAll();
		zadlCfgs = SK_List.getPage(zadlCfgs, page, size, pageCount);
		return zadlCfgs;
	}
}