package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 英雄技能
 */
public class YxjnCfg {

	public static final String TABLENAME = "英雄技能";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxjnCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YxjnCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhyx_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户英雄_id */
	private int yhyx_id;
	
	/** 技能id */
	private int jnid;
	
	/** 技能lvl */
	private int jnlvl;
	
	
	public YxjnCfg() {
		super();
	}
	
	public YxjnCfg(int id, int yhyx_id, int jnid, int jnlvl) {
		super();
		this.id = id;
		this.yhyx_id = yhyx_id;
		this.jnid = jnid;
		this.jnlvl = jnlvl;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYhyx_id() {
		return yhyx_id;
	}
	
	public void setYhyx_id(int yhyx_id) {
		this.yhyx_id = yhyx_id;
	}
	public int getJnid() {
		return jnid;
	}
	
	public void setJnid(int jnid) {
		this.jnid = jnid;
	}
	public int getJnlvl() {
		return jnlvl;
	}
	
	public void setJnlvl(int jnlvl) {
		this.jnlvl = jnlvl;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YxjnCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YxjnCfg> yxjnCfgs = new ArrayList<YxjnCfg>();
		for (Map<String, Object> map : list) {
			yxjnCfgs.add(createForColumnNameMap(map));
		}
		return yxjnCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YxjnCfg createForColumnNameMap(Map<String, Object> map){
    	YxjnCfg obj = new YxjnCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhyx_id = SK_Map.getInt("用户英雄_id", map);
	    obj.jnid = SK_Map.getInt("技能id", map);
	    obj.jnlvl = SK_Map.getInt("技能lvl", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yhyx_id);
	    SK_OutputStream.writeInt(out,this.jnid);
	    SK_OutputStream.writeInt(out,this.jnlvl);
    }
    
     public static YxjnCfg forStream(ByteArrayInputStream in) throws Exception {
     	YxjnCfg yxjnCfg = new YxjnCfg();
	    yxjnCfg.id = SK_InputStream.readInt(in,null);
	    yxjnCfg.yhyx_id = SK_InputStream.readInt(in,null);
	    yxjnCfg.jnid = SK_InputStream.readInt(in,null);
	    yxjnCfg.jnlvl = SK_InputStream.readInt(in,null);
	    return yxjnCfg;
     }
    
    public static List<YxjnCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YxjnCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YxjnCfg.TABLENAME);
	}
	
	public static List<YxjnCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YxjnCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YxjnCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName;
		List<YxjnCfg> yxjnCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxjnCfgs = YxjnCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjnCfgs;
	}
	
	public static List<YxjnCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YxjnCfg> yxjnCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yxjnCfgs.size());
			for(YxjnCfg yxjnCfg : yxjnCfgs){
				yxjnCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YxjnCfg yxjnCfg){
		idCache.put(SK_Plus.b(yxjnCfg.getId()).e(),yxjnCfg);
		Set<String> yhyx_idset = yhyx_idCache.get(String.valueOf(SK_Plus.b(yxjnCfg.getYhyx_id()).e()));
		if(yhyx_idset == null){
			yhyx_idset = new HashSet<String>();
		}
		yhyx_idset.add(String.valueOf(yxjnCfg.getId()));
		yhyx_idCache.put(SK_Plus.b(yxjnCfg.getYhyx_id()).e(),yhyx_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YxjnCfg getById(int id){
		YxjnCfg yxjnCfg = null;
		String key = SK_Plus.b(id).e();
		yxjnCfg = idCache.get(key);
		
		return yxjnCfg;
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<YxjnCfg> getByYhyx_id(int yhyx_id){
		List<YxjnCfg> yxjnCfgs = new ArrayList<YxjnCfg>();
		String key = SK_Plus.b(yhyx_id).e();
		Set<String> keys = yhyx_idCache.get(key);
		if(keys != null){
			YxjnCfg yxjnCfg = null;
			for (String k : keys) {
				yxjnCfg = getById(Integer.valueOf(k));
				if (yxjnCfg == null) continue;
					yxjnCfgs.add(yxjnCfg);
			}
		}
		return yxjnCfgs;
	}
	
	public static List<YxjnCfg> getByPageYhyx_id(int yhyx_id,int page,int size,Integer pageCount){
		List<YxjnCfg> yxjnCfgs = getByYhyx_id(yhyx_id);
		yxjnCfgs = SK_List.getPage(yxjnCfgs, page, size, pageCount);
		return yxjnCfgs;
	}
	
	public static List<YxjnCfg> getAll(){
		return new ArrayList<YxjnCfg>(idCache.values());
	}
	
	public static List<YxjnCfg> getAll(int page,int size,Integer pageCount){
		List<YxjnCfg> yxjnCfgs = getAll();
		yxjnCfgs = SK_List.getPage(yxjnCfgs, page, size, pageCount);
		return yxjnCfgs;
	}
}