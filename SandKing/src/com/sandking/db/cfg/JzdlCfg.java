package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 建筑队列
 */
public class JzdlCfg {

	public static final String TABLENAME = "建筑队列";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		JzdlCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, JzdlCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhjz_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户建筑_id */
	private int yhjz_id;
	
	
	public JzdlCfg() {
		super();
	}
	
	public JzdlCfg(int id, int yhjz_id) {
		super();
		this.id = id;
		this.yhjz_id = yhjz_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYhjz_id() {
		return yhjz_id;
	}
	
	public void setYhjz_id(int yhjz_id) {
		this.yhjz_id = yhjz_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<JzdlCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<JzdlCfg> jzdlCfgs = new ArrayList<JzdlCfg>();
		for (Map<String, Object> map : list) {
			jzdlCfgs.add(createForColumnNameMap(map));
		}
		return jzdlCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static JzdlCfg createForColumnNameMap(Map<String, Object> map){
    	JzdlCfg obj = new JzdlCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("用户建筑_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yhjz_id);
    }
    
     public static JzdlCfg forStream(ByteArrayInputStream in) throws Exception {
     	JzdlCfg jzdlCfg = new JzdlCfg();
	    jzdlCfg.id = SK_InputStream.readInt(in,null);
	    jzdlCfg.yhjz_id = SK_InputStream.readInt(in,null);
	    return jzdlCfg;
     }
    
    public static List<JzdlCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<JzdlCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,JzdlCfg.TABLENAME);
	}
	
	public static List<JzdlCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<JzdlCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<JzdlCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName;
		List<JzdlCfg> jzdlCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			jzdlCfgs = JzdlCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdlCfgs;
	}
	
	public static List<JzdlCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<JzdlCfg> jzdlCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, jzdlCfgs.size());
			for(JzdlCfg jzdlCfg : jzdlCfgs){
				jzdlCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(JzdlCfg jzdlCfg){
		idCache.put(SK_Plus.b(jzdlCfg.getId()).e(),jzdlCfg);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(jzdlCfg.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(jzdlCfg.getId()));
		yhjz_idCache.put(SK_Plus.b(jzdlCfg.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static JzdlCfg getById(int id){
		JzdlCfg jzdlCfg = null;
		String key = SK_Plus.b(id).e();
		jzdlCfg = idCache.get(key);
		
		return jzdlCfg;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<JzdlCfg> getByYhjz_id(int yhjz_id){
		List<JzdlCfg> jzdlCfgs = new ArrayList<JzdlCfg>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			JzdlCfg jzdlCfg = null;
			for (String k : keys) {
				jzdlCfg = getById(Integer.valueOf(k));
				if (jzdlCfg == null) continue;
					jzdlCfgs.add(jzdlCfg);
			}
		}
		return jzdlCfgs;
	}
	
	public static List<JzdlCfg> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<JzdlCfg> jzdlCfgs = getByYhjz_id(yhjz_id);
		jzdlCfgs = SK_List.getPage(jzdlCfgs, page, size, pageCount);
		return jzdlCfgs;
	}
	
	public static List<JzdlCfg> getAll(){
		return new ArrayList<JzdlCfg>(idCache.values());
	}
	
	public static List<JzdlCfg> getAll(int page,int size,Integer pageCount){
		List<JzdlCfg> jzdlCfgs = getAll();
		jzdlCfgs = SK_List.getPage(jzdlCfgs, page, size, pageCount);
		return jzdlCfgs;
	}
}