package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 聊天
 */
public class LtCfg {

	public static final String TABLENAME = "聊天";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, LtCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> ltlx_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> lm_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> jsridCache = SK_Collections.newMap();
	static final Map<String, Set<String>> fyridCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 联盟id */
	private String lmid;
	
	/** 内容 */
	private String nr;
	
	/** 聊天类型_id */
	private int ltlx_id;
	
	/** 联盟_id */
	private int lm_id;
	
	/** 接收人id */
	private int jsrid;
	
	/** 发言人id */
	private int fyrid;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	
	public LtCfg() {
		super();
	}
	
	public LtCfg(int id, String lmid, String nr, int ltlx_id, int lm_id, int jsrid, int fyrid, java.util.Date cjsj) {
		super();
		this.id = id;
		this.lmid = lmid;
		this.nr = nr;
		this.ltlx_id = ltlx_id;
		this.lm_id = lm_id;
		this.jsrid = jsrid;
		this.fyrid = fyrid;
		this.cjsj = cjsj;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getLmid() {
		return lmid;
	}
	
	public void setLmid(String lmid) {
		this.lmid = lmid;
	}
	public String getNr() {
		return nr;
	}
	
	public void setNr(String nr) {
		this.nr = nr;
	}
	public int getLtlx_id() {
		return ltlx_id;
	}
	
	public void setLtlx_id(int ltlx_id) {
		this.ltlx_id = ltlx_id;
	}
	public int getLm_id() {
		return lm_id;
	}
	
	public void setLm_id(int lm_id) {
		this.lm_id = lm_id;
	}
	public int getJsrid() {
		return jsrid;
	}
	
	public void setJsrid(int jsrid) {
		this.jsrid = jsrid;
	}
	public int getFyrid() {
		return fyrid;
	}
	
	public void setFyrid(int fyrid) {
		this.fyrid = fyrid;
	}
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<LtCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<LtCfg> ltCfgs = new ArrayList<LtCfg>();
		for (Map<String, Object> map : list) {
			ltCfgs.add(createForColumnNameMap(map));
		}
		return ltCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static LtCfg createForColumnNameMap(Map<String, Object> map){
    	LtCfg obj = new LtCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.lmid = SK_Map.getString("联盟id", map);
	    obj.nr = SK_Map.getString("内容", map);
	    obj.ltlx_id = SK_Map.getInt("聊天类型_id", map);
	    obj.lm_id = SK_Map.getInt("联盟_id", map);
	    obj.jsrid = SK_Map.getInt("接收人id", map);
	    obj.fyrid = SK_Map.getInt("发言人id", map);
	    obj.cjsj = SK_Map.getDate("创建时间", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.lmid);
	    SK_OutputStream.writeString(out,this.nr);
	    SK_OutputStream.writeInt(out,this.ltlx_id);
	    SK_OutputStream.writeInt(out,this.lm_id);
	    SK_OutputStream.writeInt(out,this.jsrid);
	    SK_OutputStream.writeInt(out,this.fyrid);
	    SK_OutputStream.writeDate(out,this.cjsj);
    }
    
     public static LtCfg forStream(ByteArrayInputStream in) throws Exception {
     	LtCfg ltCfg = new LtCfg();
	    ltCfg.id = SK_InputStream.readInt(in,null);
	    ltCfg.lmid = SK_InputStream.readString(in,null);
	    ltCfg.nr = SK_InputStream.readString(in,null);
	    ltCfg.ltlx_id = SK_InputStream.readInt(in,null);
	    ltCfg.lm_id = SK_InputStream.readInt(in,null);
	    ltCfg.jsrid = SK_InputStream.readInt(in,null);
	    ltCfg.fyrid = SK_InputStream.readInt(in,null);
	    ltCfg.cjsj = SK_InputStream.readDate(in,null);
	    return ltCfg;
     }
    
    public static List<LtCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<LtCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,LtCfg.TABLENAME);
	}
	
	public static List<LtCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<LtCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<LtCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName;
		List<LtCfg> ltCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			ltCfgs = LtCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return ltCfgs;
	}
	
	public static List<LtCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<LtCfg> ltCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, ltCfgs.size());
			for(LtCfg ltCfg : ltCfgs){
				ltCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(LtCfg ltCfg){
		idCache.put(SK_Plus.b(ltCfg.getId()).e(),ltCfg);
		Set<String> ltlx_idset = ltlx_idCache.get(String.valueOf(SK_Plus.b(ltCfg.getLtlx_id()).e()));
		if(ltlx_idset == null){
			ltlx_idset = new HashSet<String>();
		}
		ltlx_idset.add(String.valueOf(ltCfg.getId()));
		ltlx_idCache.put(SK_Plus.b(ltCfg.getLtlx_id()).e(),ltlx_idset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(ltCfg.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(ltCfg.getId()));
		lm_idCache.put(SK_Plus.b(ltCfg.getLm_id()).e(),lm_idset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(ltCfg.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(ltCfg.getId()));
		jsridCache.put(SK_Plus.b(ltCfg.getJsrid()).e(),jsridset);
		Set<String> fyridset = fyridCache.get(String.valueOf(SK_Plus.b(ltCfg.getFyrid()).e()));
		if(fyridset == null){
			fyridset = new HashSet<String>();
		}
		fyridset.add(String.valueOf(ltCfg.getId()));
		fyridCache.put(SK_Plus.b(ltCfg.getFyrid()).e(),fyridset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static LtCfg getById(int id){
		LtCfg ltCfg = null;
		String key = SK_Plus.b(id).e();
		ltCfg = idCache.get(key);
		
		return ltCfg;
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<LtCfg> getByLtlx_id(int ltlx_id){
		List<LtCfg> ltCfgs = new ArrayList<LtCfg>();
		String key = SK_Plus.b(ltlx_id).e();
		Set<String> keys = ltlx_idCache.get(key);
		if(keys != null){
			LtCfg ltCfg = null;
			for (String k : keys) {
				ltCfg = getById(Integer.valueOf(k));
				if (ltCfg == null) continue;
					ltCfgs.add(ltCfg);
			}
		}
		return ltCfgs;
	}
	
	public static List<LtCfg> getByPageLtlx_id(int ltlx_id,int page,int size,Integer pageCount){
		List<LtCfg> ltCfgs = getByLtlx_id(ltlx_id);
		ltCfgs = SK_List.getPage(ltCfgs, page, size, pageCount);
		return ltCfgs;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<LtCfg> getByLm_id(int lm_id){
		List<LtCfg> ltCfgs = new ArrayList<LtCfg>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			LtCfg ltCfg = null;
			for (String k : keys) {
				ltCfg = getById(Integer.valueOf(k));
				if (ltCfg == null) continue;
					ltCfgs.add(ltCfg);
			}
		}
		return ltCfgs;
	}
	
	public static List<LtCfg> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<LtCfg> ltCfgs = getByLm_id(lm_id);
		ltCfgs = SK_List.getPage(ltCfgs, page, size, pageCount);
		return ltCfgs;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<LtCfg> getByJsrid(int jsrid){
		List<LtCfg> ltCfgs = new ArrayList<LtCfg>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			LtCfg ltCfg = null;
			for (String k : keys) {
				ltCfg = getById(Integer.valueOf(k));
				if (ltCfg == null) continue;
					ltCfgs.add(ltCfg);
			}
		}
		return ltCfgs;
	}
	
	public static List<LtCfg> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<LtCfg> ltCfgs = getByJsrid(jsrid);
		ltCfgs = SK_List.getPage(ltCfgs, page, size, pageCount);
		return ltCfgs;
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<LtCfg> getByFyrid(int fyrid){
		List<LtCfg> ltCfgs = new ArrayList<LtCfg>();
		String key = SK_Plus.b(fyrid).e();
		Set<String> keys = fyridCache.get(key);
		if(keys != null){
			LtCfg ltCfg = null;
			for (String k : keys) {
				ltCfg = getById(Integer.valueOf(k));
				if (ltCfg == null) continue;
					ltCfgs.add(ltCfg);
			}
		}
		return ltCfgs;
	}
	
	public static List<LtCfg> getByPageFyrid(int fyrid,int page,int size,Integer pageCount){
		List<LtCfg> ltCfgs = getByFyrid(fyrid);
		ltCfgs = SK_List.getPage(ltCfgs, page, size, pageCount);
		return ltCfgs;
	}
	
	public static List<LtCfg> getAll(){
		return new ArrayList<LtCfg>(idCache.values());
	}
	
	public static List<LtCfg> getAll(int page,int size,Integer pageCount){
		List<LtCfg> ltCfgs = getAll();
		ltCfgs = SK_List.getPage(ltCfgs, page, size, pageCount);
		return ltCfgs;
	}
}