package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户障碍
 */
public class YhzaCfg {

	public static final String TABLENAME = "用户障碍";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzaCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhzaCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 障碍id */
	private int zaid;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public YhzaCfg() {
		super();
	}
	
	public YhzaCfg(int id, int zaid, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.zaid = zaid;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getZaid() {
		return zaid;
	}
	
	public void setZaid(int zaid) {
		this.zaid = zaid;
	}
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhzaCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhzaCfg> yhzaCfgs = new ArrayList<YhzaCfg>();
		for (Map<String, Object> map : list) {
			yhzaCfgs.add(createForColumnNameMap(map));
		}
		return yhzaCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhzaCfg createForColumnNameMap(Map<String, Object> map){
    	YhzaCfg obj = new YhzaCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.zaid = SK_Map.getInt("障碍id", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.zaid);
	    SK_OutputStream.writeInt(out,this.x);
	    SK_OutputStream.writeInt(out,this.y);
	    SK_OutputStream.writeInt(out,this.yh_id);
    }
    
     public static YhzaCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhzaCfg yhzaCfg = new YhzaCfg();
	    yhzaCfg.id = SK_InputStream.readInt(in,null);
	    yhzaCfg.zaid = SK_InputStream.readInt(in,null);
	    yhzaCfg.x = SK_InputStream.readInt(in,null);
	    yhzaCfg.y = SK_InputStream.readInt(in,null);
	    yhzaCfg.yh_id = SK_InputStream.readInt(in,null);
	    return yhzaCfg;
     }
    
    public static List<YhzaCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhzaCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhzaCfg.TABLENAME);
	}
	
	public static List<YhzaCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhzaCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhzaCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,障碍id,x,y,用户_id FROM " + tableName;
		List<YhzaCfg> yhzaCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhzaCfgs = YhzaCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzaCfgs;
	}
	
	public static List<YhzaCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhzaCfg> yhzaCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhzaCfgs.size());
			for(YhzaCfg yhzaCfg : yhzaCfgs){
				yhzaCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhzaCfg yhzaCfg){
		idCache.put(SK_Plus.b(yhzaCfg.getId()).e(),yhzaCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzaCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhzaCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhzaCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhzaCfg getById(int id){
		YhzaCfg yhzaCfg = null;
		String key = SK_Plus.b(id).e();
		yhzaCfg = idCache.get(key);
		
		return yhzaCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhzaCfg> getByYh_id(int yh_id){
		List<YhzaCfg> yhzaCfgs = new ArrayList<YhzaCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhzaCfg yhzaCfg = null;
			for (String k : keys) {
				yhzaCfg = getById(Integer.valueOf(k));
				if (yhzaCfg == null) continue;
					yhzaCfgs.add(yhzaCfg);
			}
		}
		return yhzaCfgs;
	}
	
	public static List<YhzaCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhzaCfg> yhzaCfgs = getByYh_id(yh_id);
		yhzaCfgs = SK_List.getPage(yhzaCfgs, page, size, pageCount);
		return yhzaCfgs;
	}
	
	public static List<YhzaCfg> getAll(){
		return new ArrayList<YhzaCfg>(idCache.values());
	}
	
	public static List<YhzaCfg> getAll(int page,int size,Integer pageCount){
		List<YhzaCfg> yhzaCfgs = getAll();
		yhzaCfgs = SK_List.getPage(yhzaCfgs, page, size, pageCount);
		return yhzaCfgs;
	}
}