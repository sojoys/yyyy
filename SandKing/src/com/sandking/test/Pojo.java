package com.sandking.test;

import java.util.Date;
import java.util.List;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月27日 下午12:01:33
 * @Description ：Please describe this document
 */
public class Pojo {
	private int id;
	private String name;
	private String card;
	private String pass;
	private int age;
	private Date crateTame;
	private String info;
	private User user;
	private List<User> users;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getCrateTame() {
		return crateTame;
	}

	public void setCrateTame(Date crateTame) {
		this.crateTame = crateTame;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Pojo() {
	}

	public Pojo(int id, String name, String card, String pass, int age,
			Date crateTame, String info, User user, List<User> users) {
		super();
		this.id = id;
		this.name = name;
		this.card = card;
		this.pass = pass;
		this.age = age;
		this.crateTame = crateTame;
		this.info = info;
		this.user = user;
		this.users = users;
	}
}
