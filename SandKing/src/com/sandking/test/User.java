package com.sandking.test;

import java.util.List;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月27日 下午12:41:07
 * @Description ：Please describe this document
 */
public class User {
	private int id;
	private String name;
	public List<String> server;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getServer() {
		return server;
	}

	public void setServer(List<String> server) {
		this.server = server;
	}

	public User(int id, String name, List<String> server) {
		super();
		this.id = id;
		this.name = name;
		this.server = server;
	}

	public User() {
		super();
	}
}
