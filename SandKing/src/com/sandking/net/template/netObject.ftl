package ${packageName};

<#list netObjImports as imports>
import ${imports};
</#list>
 

/**
 * ${remark}
 */
public class ${className}{
	<#list fields as field>
	/** ${field.remark} */
	private ${field.type} ${field.name};
	</#list>
	
	<#list fields as field>
	public ${field.type} get${field.dname}(){
		return ${field.name};
	}
	
	public void set${field.dname}(${field.type} ${field.xname}){
		this.${field.name} = ${field.xname};
	}
	
	</#list>
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        <#list fields as field>
        result.put("${field.name}",${field.xname});
        </#list>
        return result;
    }
}