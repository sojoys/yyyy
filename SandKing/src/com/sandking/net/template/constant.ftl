package ${packageName};
/**
 * ${remark}
 */
public class ${dclassName}{
	<#list fields as field>
	/** ${field.remark} */
	<#if field.type == "String" >
	public static final ${field.type} ${field.name} = "${field.values}";
	<#else>
	public static final ${field.type} ${field.name} = ${field.values};
	</#if>
	</#list>
}