package com.sandking.net;

import freemarker.template.Configuration;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月30日 上午11:50:22
 * @Description ：Please describe this document
 */
public class NetConfig {
	/** 文件生成路径 */
	private String filePath;
	
	/** 服务器接口生成路径 */
	private String serverImpPath;
	
	/** 客户端接口生成路径 */
	private String clientImpPath;
	
	/** 常量对象生成路径 */
	private String constantPath;
	
	/** 网络对象生成路径 */
	private String netObjectPath;
	
	/** 生成服务器接口名称 */
	private String serverImpName;
	
	/** 生成客户端接口名称 */
	private String clientImpName;
	
	/** 模板config */
	private Configuration configuration;
	
	/** 服务器接口对象 */
	private Class<?> serverImp;
	
	/** 常量对象 */
	private Class<?> constant;
	
	/** 网络传输对象 */
	private Class<?> netObject;
	
	/** 客户端接口对象 */
	private Class<?> clientImp;

	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getServerImpPath() {
		return serverImpPath;
	}

	public void setServerImpPath(String serverImpPath) {
		this.serverImpPath = serverImpPath;
	}

	public String getClientImpPath() {
		return clientImpPath;
	}

	public void setClientImpPath(String clientImpPath) {
		this.clientImpPath = clientImpPath;
	}

	public String getConstantPath() {
		return constantPath;
	}

	public void setConstantPath(String constantPath) {
		this.constantPath = constantPath;
	}

	public String getNetObjectPath() {
		return netObjectPath;
	}

	public void setNetObjectPath(String netObjectPath) {
		this.netObjectPath = netObjectPath;
	}

	public String getServerImpName() {
		return serverImpName;
	}

	public void setServerImpName(String serverImpName) {
		this.serverImpName = serverImpName;
	}

	public String getClientImpName() {
		return clientImpName;
	}

	public void setClientImpName(String clientImpName) {
		this.clientImpName = clientImpName;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Class<?> getServerImp() {
		return serverImp;
	}

	public void setServerImp(Class<?> serverImp) {
		this.serverImp = serverImp;
	}

	public Class<?> getConstant() {
		return constant;
	}

	public void setConstant(Class<?> constant) {
		this.constant = constant;
	}

	public Class<?> getNetObject() {
		return netObject;
	}

	public void setNetObject(Class<?> netObject) {
		this.netObject = netObject;
	}

	public Class<?> getClientImp() {
		return clientImp;
	}

	public void setClientImp(Class<?> clientImp) {
		this.clientImp = clientImp;
	}

}