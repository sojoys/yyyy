package com.sandking.net.pojo;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午4:59:32
 * @Description ：Please describe this document
 */
public class NetField {
	private String type;
	private String name;
	private String dname;
	private String xname;
	private String values;
	private String remark;

	public NetField(String type, String name, String dname, String xname,
			String values, String remark) {
		super();
		this.type = type;
		this.name = name;
		this.dname = dname;
		this.xname = xname;
		this.values = values;
		this.remark = remark;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getXname() {
		return xname;
	}

	public void setXname(String xname) {
		this.xname = xname;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
