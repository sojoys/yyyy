package com.sandking.net.imp;


/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午4:55:24
 * @Description ：网络层生成器描述注解
 */
public @interface NetAnnotation {
	/** 备注 */
	String remark();
}
