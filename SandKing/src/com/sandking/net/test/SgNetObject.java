package com.sandking.net.test;

import com.sandking.net.imp.NetAnnotation;
import com.sandking.net.imp.NetObject;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午5:08:47
 * @Description ：Please describe this document
 */
public class SgNetObject extends NetObject {
	@NetAnnotation(remark = "用户类型")
	class User {
		@NetAnnotation(remark = "id")
		int id;
		@NetAnnotation(remark = "用户昵称")
		String name;
	}
}
