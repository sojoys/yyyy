package com.sandking.net.test;

import com.sandking.net.imp.NetAnnotation;
import com.sandking.net.imp.NetReturn;
import com.sandking.net.imp.ServerImp;
import com.sandking.net.test.SgNetObject.User;
import com.sandking.net.imp.NetObject.ReturnStatus;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月30日 上午11:32:49
 * @Description ：Please describe this document
 */
@NetAnnotation(remark = "服务器接口")
public abstract class SgServerImp extends ServerImp {

	@NetAnnotation(remark = "登录")
	public static ReturnStatus a(String zh, String mm, @NetReturn(remark = "用户") User user){
		return null;
	};
	
	@NetAnnotation(remark = "移动")
	public static ReturnStatus b(String zh, String mm){
		return null;
	};
	
	@NetAnnotation(remark = "登录")
	public static ReturnStatus c(@NetReturn(remark = "用户") User user){
		return null;
	};
	
	@NetAnnotation(remark = "登录")
	public static void d(String zh, String mm, @NetReturn(remark = "用户") User user){
	};
	
	@NetAnnotation(remark = "移动")
	public static void e(String zh, String mm){
	};
	
	@NetAnnotation(remark = "登录")
	public static void f(@NetReturn(remark = "用户") User user){
	};
}
