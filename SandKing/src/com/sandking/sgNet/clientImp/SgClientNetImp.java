package com.sandking.sgNet.clientImp;
/**
 * 登录
 */
 
import com.sandking.tools.SK_Map;
import java.util.Map;
import com.sandking.sgNet.netObject.User;
import com.sandking.net.SK_ChannelImp;
import java.util.HashMap;
 
public abstract class SgClientNetImp {

	// 逻辑分发
	public void disp(SK_ChannelImp skc, Map<Object,Object> map) throws Exception {
        if(skc == null) return;
        String cmd  = SK_Map.getString("c", map);
        Map<Object, Object> p  = SK_Map.getMap("p", map);
        switch (cmd) {
		case "pushMail": // 登录
			pushMail(skc,p);
			break;
		default:
			break;
		}
    }

	
	/** 登录 */
	private void pushMail(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
		
		// 返回
		User user = null;
		//执行逻辑实现
		pushMail(skc,user);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		p.put("user",user.toMap());
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


	// 需要实现的接口
	
	/** 登录 */
	public abstract void pushMail(SK_ChannelImp skc,User user);
	
}