package com.sandking.tools;

/**
 * @UserName : SandKing
 * @DataTime : 2014年3月25日 下午4:25:47
 * @Description ：字符串拼接
 */
public class SK_Plus {
	private StringBuffer sb = null;

	private SK_Plus() {
		sb = new StringBuffer();
	}

	public static SK_Plus newSK_Plus() {
		return new SK_Plus();
	}

	public SK_Plus a(Object object) {
		if (sb != null) {
			sb.append(object);
		}
		return this;
	}

	public SK_Plus as(Object... objects) {
		for (Object object : objects) {
			this.a(object);
		}
		return this;
	}

	public static SK_Plus b(Object... objects) {
		SK_Plus plus = new SK_Plus();
		return plus.as(objects);
	}

	public String e() {
		String ret = "";
		if (sb != null) {
			ret = sb.toString();
		}
		sb = null;
		return ret;
	}
}
